#include "pch.h"
#include "MiniginMain.h"

#include "SceneManager.h"
#include "Scene.h"

#include "Entity.h"
#include "Components.h"
#include "Commands.h"
#include "CharacterClasses.h"
#include "CollisionClasses.h"
#include "Texture2D.h"
#include "Utils.h"
#include "Time.h"
#include "InputManager.h"
#include "BinaryReader.h"

#pragma region Macros
//Makes a shared Entity pointer and adds it to the scene (stored in var pEntityName)
#define ADD(pEntityName, sceneRef) std::shared_ptr<Budgety::Entity> pEntityName = sceneRef.AddEntity
//Makes a shared pointer
#define PTR(Type) std::make_shared<Type>
//Casts smart pointer to Type
#define CAST(Type) std::dynamic_pointer_cast<Type>
//Calls InputManager::AddCommand()
#define ADDCOMMAND Budgety::InputManager::GetInstance().AddCommand
//Makes a shared pointer to a derived Command class and casts it to a Command pointer (stored in var pCommandName)
#define MAKECOMMANDPTR(Type, pCommandName) std::shared_ptr<Budgety::Command> pCommandName = CAST(Budgety::Command)(PTR(Type)())
#pragma endregion Macros

constexpr float g_Offset = 5.f;
constexpr float g_Speed = 100.f;
constexpr float g_JumpStrength = 270.f;
constexpr float g_CharacterMass = 40.f, g_TerminalVelocity = 115.f;

unsigned int g_LevelIdx = 5;

void LoadGame()
{
	using namespace Budgety;
	using Type = InputManager::TriggerType;

	auto& sceneManager = SceneManager::GetInstance();
	auto& singlePlayerScene = sceneManager.CreateScene("Single player");
	sceneManager.SetActiveScene("Single player");

	std::shared_ptr<Texture2DComponent> pTex2DComp;
#pragma warning(push)
	//avoid unnamed objects
#pragma warning (disable:26444)

	//player1 Entity
	ADD(pPlayer1Entity, singlePlayerScene)(true);
	pPlayer1Entity->AddComponent(PTR(TransformComponent)(player1SpawnPosX, playerSpawnPosY));
	pTex2DComp = pPlayer1Entity->AddComponent(PTR(Texture2DComponent)("sprites.png", SPRITESIZE - 1, SPRITESIZE, glm::vec2{ 0.5f, 0.5f }, SCALE));
	pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * 0, 18); //Movingright = 0
	pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * 1, 18); //Movingleft = 1
	pTex2DComp->AddAnimation(4, SPRITESIZE, SPRITESIZE, 0.1f, SPRITESIZE * 20, 18); //Shootright
	pTex2DComp->AddAnimation(4, SPRITESIZE, SPRITESIZE, 0.1f, SPRITESIZE * 21, 18); //Shootleft
	pTex2DComp->AddAnimation(4, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * 47, 16); //Death
	pPlayer1Entity->AddComponent(CAST(CharacterComponent)(PTR(PlayerCharacterComponent)(g_Speed, g_JumpStrength)));
	pPlayer1Entity->AddComponent(PTR(PhysicsComponent)(g_CharacterMass, g_TerminalVelocity));
	pPlayer1Entity->AddComponent(CAST(CollisionComponent)(PTR(PlayerCollisionComponent)(pTex2DComp)));

	//player2 Entity
	ADD(pPlayer2Entity, singlePlayerScene)(true);
	pPlayer2Entity->AddComponent(PTR(TransformComponent)(player2SpawnPosX, playerSpawnPosY));
	pTex2DComp = pPlayer2Entity->AddComponent(PTR(Texture2DComponent)("sprites.png", SPRITESIZE - 1, SPRITESIZE, glm::vec2{ 0.5f, 0.5f }, SCALE));
	pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * 2, 18); //Movingright = 0
	pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * 3, 18); //Movingleft = 1
	pTex2DComp->AddAnimation(4, SPRITESIZE, SPRITESIZE, 0.1f, SPRITESIZE * 24, 18); //Shootright
	pTex2DComp->AddAnimation(4, SPRITESIZE, SPRITESIZE, 0.1f, SPRITESIZE * 25, 18); //Shootleft
	pTex2DComp->AddAnimation(4, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * 50, 16); //Death
	pPlayer2Entity->AddComponent(CAST(CharacterComponent)(PTR(PlayerCharacterComponent)(g_Speed, g_JumpStrength)));
	pPlayer2Entity->AddComponent(PTR(PhysicsComponent)(g_CharacterMass, g_TerminalVelocity));
	pPlayer2Entity->AddComponent(CAST(CollisionComponent)(PTR(PlayerCollisionComponent)(pTex2DComp)));

	//blocks
	std::shared_ptr<Entity> pBlockEntity;
	std::array<LevelData, AMOUNT_OF_LEVELS> levelDatas = BinaryReader::ReadLevels("../Data/SeperatedLevelData.dat");
	int srcRectX = (g_LevelIdx % 10) * BLOCKSIZE, srcRectY = (g_LevelIdx / 10) * BLOCKSIZE;

	//enemies
	std::shared_ptr<Entity> pEnemyEntity;
	std::array<std::vector<EnemyData>, AMOUNT_OF_LEVELS> allLevelEnemyDatas = BinaryReader::ReadEnemies("../Data/SeperatedEnemyData.dat");

	for (EnemyData& enemyData : allLevelEnemyDatas[g_LevelIdx])
	{
		//enemy
		int spriteOffset1{}, spriteOffset2{}, spriteOffset3{};

		if (enemyData.type == EnemyType::ZenChan)
		{
			spriteOffset1 = 4;
			spriteOffset2 = 52;
			spriteOffset3 = 30;
		}
		else //enemyData.type == EnemyType::Maita
		{
			spriteOffset1 = 15;
			spriteOffset2 = 53;
			spriteOffset3 = 36;
		}

		pEnemyEntity = singlePlayerScene.AddEntity(true);
		pEnemyEntity->AddComponent(PTR(TransformComponent)(enemyData.pos));
		pTex2DComp = pEnemyEntity->AddComponent(PTR(Texture2DComponent)("sprites.png", SPRITESIZE - 1, SPRITESIZE, glm::vec2{ 0.5f, 0.5f }, SCALE));
		pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * spriteOffset1++, 18); //Movingright = 0
		pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * spriteOffset1, 18); //Movingleft = 1
		pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * spriteOffset2, 20); //Death
		pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * spriteOffset3, 18); //InBubble
		pEnemyEntity->AddComponent(CAST(CharacterComponent)(PTR(EnemyCharacterComponent)(g_Speed / 2.f, g_JumpStrength)));
		pEnemyEntity->AddComponent(PTR(PhysicsComponent)(g_CharacterMass, g_TerminalVelocity));
		pEnemyEntity->AddComponent(CAST(CollisionComponent)(PTR(EnemyCollisionComponent)(pTex2DComp)));
		pEnemyEntity->AddComponent(PTR(AIComponent)(std::vector<const Entity*>{pPlayer1Entity.get(), pPlayer2Entity.get()}));
	}

	for (int blockTransformIdx{}; blockTransformIdx < levelDatas[g_LevelIdx].size(); blockTransformIdx++)
	{
		//block
		pBlockEntity = singlePlayerScene.AddEntity();
		pBlockEntity->AddComponent(PTR(TransformComponent)(levelDatas[g_LevelIdx][blockTransformIdx]));
		pTex2DComp = pBlockEntity->AddComponent(PTR(Texture2DComponent)("blocks.png", BLOCKSIZE, BLOCKSIZE, glm::vec2{ 0.f, 0.f }, SCALE));
		pTex2DComp->SetSrcRectPos(srcRectX, srcRectY);
		pBlockEntity->AddComponent(CAST(CollisionComponent)(PTR(NoCollisionBehaviourComponent)(CollisionGroup::Ground, pTex2DComp)));
	}

	ADD(pTextEntity, singlePlayerScene)();
	pTextEntity->AddComponent(PTR(TransformComponent)(WINDOW_WIDTH / 2.f, g_Offset));
	pTextEntity->AddComponent(PTR(TextComponent)("Bubble Bobble", "ARCADECLASSIC.otf", 36, glm::vec2{ 0.5f, 0.f }));

	ADD(pFPSEntity, singlePlayerScene)();
	pFPSEntity->AddComponent(PTR(TransformComponent)(g_Offset, g_Offset));
	auto pFPSComp = pFPSEntity->AddComponent(PTR(FPSComponent)());
	pFPSComp->AddTextComponent(PTR(TextComponent)("0", "Lingua.otf", 32))->SetColor(255, 255, 0);

	//commands
	MAKECOMMANDPTR(TogglePauseCommand, pTogglePauseCommand);

	MAKECOMMANDPTR(MoveRightCommand, pMoveRightCommand);
	MAKECOMMANDPTR(MoveLeftCommand, pMoveLeftCommand);
	MAKECOMMANDPTR(JumpCommand, pJumpCommand);
	MAKECOMMANDPTR(ShootCommand, pShootCommand);

	//Pause
	ADDCOMMAND(SDL_SCANCODE_ESCAPE, Type::Press, pTogglePauseCommand, pPlayer1Entity.get());
	ADDCOMMAND(0, Button::Start, Type::Press, pTogglePauseCommand, pPlayer1Entity.get());
	ADDCOMMAND(1, Button::Start, Type::Press, pTogglePauseCommand, pPlayer1Entity.get());

	//player1 controls
	ADDCOMMAND(SDL_SCANCODE_D, Type::Hold, pMoveRightCommand, pPlayer1Entity.get());
	ADDCOMMAND(0, Button::DPadRight, Type::Hold, pMoveRightCommand, pPlayer1Entity.get());

	ADDCOMMAND(SDL_SCANCODE_A, Type::Hold, pMoveLeftCommand, pPlayer1Entity.get());
	ADDCOMMAND(0, Button::DPadLeft, Type::Hold, pMoveLeftCommand, pPlayer1Entity.get());

	ADDCOMMAND(SDL_SCANCODE_W, Type::Press, pJumpCommand, pPlayer1Entity.get());
	ADDCOMMAND(0, Button::ButtonA, Type::Press, pJumpCommand, pPlayer1Entity.get());

	ADDCOMMAND(SDL_SCANCODE_LCTRL, Type::Press, pShootCommand, pPlayer1Entity.get());
	ADDCOMMAND(0, Button::ButtonB, Type::Press, pShootCommand, pPlayer1Entity.get());

	//player2 controls
	ADDCOMMAND(SDL_SCANCODE_RIGHT, Type::Hold, pMoveRightCommand, pPlayer2Entity.get());
	ADDCOMMAND(1, Button::DPadRight, Type::Hold, pMoveRightCommand, pPlayer2Entity.get());

	ADDCOMMAND(SDL_SCANCODE_LEFT, Type::Hold, pMoveLeftCommand, pPlayer2Entity.get());
	ADDCOMMAND(1, Button::DPadLeft, Type::Hold, pMoveLeftCommand, pPlayer2Entity.get());

	ADDCOMMAND(SDL_SCANCODE_UP, Type::Press, pJumpCommand, pPlayer2Entity.get());
	ADDCOMMAND(1, Button::ButtonA, Type::Press, pJumpCommand, pPlayer2Entity.get());

	ADDCOMMAND(SDL_SCANCODE_RCTRL, Type::Press, pShootCommand, pPlayer2Entity.get());
	ADDCOMMAND(1, Button::ButtonB, Type::Press, pShootCommand, pPlayer2Entity.get());
#pragma warning(pop)
}

int main(int, char* [])
{
	Budgety::Run(LoadGame);

	return 0;
}