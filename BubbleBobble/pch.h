#pragma once
#include <stdio.h>
#include <iostream> // std::cout
#include <sstream> // stringstream
#include <memory> // smart pointers
#include <vector>

#pragma warning(push)
//nonstandard extension used : nameless struct/union
#pragma warning (disable : 4201)
#pragma warning (disable : 26812)
#pragma warning (disable : 26495)
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#pragma warning(pop)

#include <windows.h>

#include "Time.h"