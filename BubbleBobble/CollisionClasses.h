#pragma once
#include "Components.h"
#include "Entity.h"

class NoCollisionBehaviourComponent final : public Budgety::CollisionComponent
{
public:
	explicit NoCollisionBehaviourComponent(Budgety::CollisionGroup group, const std::shared_ptr<Budgety::Texture2DComponent>& pTextureComp) : Budgety::CollisionComponent(group, pTextureComp) {}
	explicit NoCollisionBehaviourComponent(Budgety::CollisionGroup group, const glm::vec2& dimensions, const glm::vec2& pivot) : Budgety::CollisionComponent(group, dimensions, pivot) {}
	explicit NoCollisionBehaviourComponent(Budgety::CollisionGroup group, int width, int height, const glm::vec2& pivot) : Budgety::CollisionComponent(group, width, height, pivot) {}
	~NoCollisionBehaviourComponent() = default;

	void OnCollisionEnter(Budgety::Entity*, Budgety::Side) override {}
	void OnCollisionExit(Budgety::Entity*) override {}
};

class CharacterCollisionComponent : public Budgety::CollisionComponent
{
public:
	explicit CharacterCollisionComponent(Budgety::CollisionGroup group, const std::shared_ptr<Budgety::Texture2DComponent>& pTextureComp) : Budgety::CollisionComponent(group, pTextureComp) {}
	explicit CharacterCollisionComponent(Budgety::CollisionGroup group, const glm::vec2& dimensions, const glm::vec2& pivot) : Budgety::CollisionComponent(group, dimensions, pivot) {}
	explicit CharacterCollisionComponent(Budgety::CollisionGroup group, int width, int height, const glm::vec2& pivot) : Budgety::CollisionComponent(group, width, height, pivot) {}
	virtual ~CharacterCollisionComponent() = default;

	virtual void OnCollisionEnter(Budgety::Entity* pOtherEntity, Budgety::Side enterSide) override
	{
		using namespace Budgety;
		
		CollisionComponent::OnCollisionEnter(pOtherEntity, enterSide);

		auto pOtherCollisionComp = pOtherEntity->GetComponent<CollisionComponent>();
		CollisionGroup otherGroup = pOtherCollisionComp->GetCollisionGroup();

		switch(otherGroup)
		{
		//Collide with ground
		case CollisionGroup::Ground:
		{
			auto pEntity = GetEntity();
			auto pCharComp = pEntity->GetComponent<CharacterComponent>();
			const SDL_Rect& rect = GetRect();
			const SDL_Rect& otherRect = pOtherCollisionComp->GetRect();
				
			bool keepWalking = false;
			int standOnGroundMargin = -1;
			switch (pCharComp->GetState())
			{
			case CharacterComponent::State::Grounded:
				//if the top of the new ground is as high as what you stand on
				if (rect.y + rect.h == otherRect.y - standOnGroundMargin)
					keepWalking = true;
				break;

			case CharacterComponent::State::Airborn:
				if(otherRect.y > int(levelDisplacementY)) // not the top row of blocks
				{
					//ground enters bottom of this
					if (enterSide == Side::Bottom)
					{
						TeleportOutOfBlock(pEntity, otherRect, enterSide, float(standOnGroundMargin));
						pCharComp->SetState(CharacterComponent::State::Grounded);
					}
				}
				break;
			}
			if(!keepWalking)
			{
				if (enterSide == Side::Left || enterSide == Side::Right)
				{
					TeleportOutOfBlock(pEntity, pOtherCollisionComp->GetRect(), enterSide, 1.f);
					OnCollisionExit(pOtherEntity);
					pOtherCollisionComp->OnCollisionExit(pEntity);
				}
			}
		}
			break;
		}
	}
	
	virtual void OnCollisionExit(Budgety::Entity* pOtherEntity) override
	{
		using namespace Budgety;
		
		CollisionGroup otherGroup = pOtherEntity->GetComponent<CollisionComponent>()->GetCollisionGroup();

		//expired pointers & pOtherEntity get deleted in base class funcion
		CollisionComponent::OnCollisionExit(pOtherEntity);

		switch(otherGroup)
		{
		//Exiting ground
		case CollisionGroup::Ground:
		{
			auto pCharComp = GetEntity()->GetComponent<CharacterComponent>();
				
			switch (pCharComp->GetState())
			{
			//if grounded
			case CharacterComponent::State::Grounded:
				auto it = std::find_if(m_pEnteredEntities.cbegin(), m_pEnteredEntities.cend(),
					[this](const Entity* pOtherEntity)
					{
						const SDL_Rect& rect = GetRect();
						auto pOtherCollComp = pOtherEntity->GetComponent<CollisionComponent>();
						//if ground and top of ground under top of this
						if (pOtherCollComp->GetCollisionGroup() == CollisionGroup::Ground)
							return pOtherCollComp->GetRect().y > rect.y + SPRITESIZE / 2;

						return false;
					});
				//if no other standing ground found -> AirBorn
				if (it == m_pEnteredEntities.end())
					pCharComp->SetState(CharacterComponent::State::Airborn);
				break;
			}
		}
			break;
		}
	}

private:
	void TeleportOutOfBlock(Budgety::Entity* pEntity, const SDL_Rect& pOtherRect, Budgety::Side side, float margin, bool stopVelocity = true) const
	{
		using namespace Budgety;

		auto pPhysicsComp = pEntity->GetComponent<PhysicsComponent>();
		auto pTransform = pEntity->GetComponent<TransformComponent>();
		glm::vec3 oldPos; //new x or y pos
		float newPos; //new x or y pos
		float direction = 0.f;
		glm::vec3 currVel;
		
		switch (side) //side where other object touched this
		{
		case Side::Top:
			direction = 1.f;
		case Side::Bottom:
			//get new y position and transform to it
			newPos = pOtherRect.y + direction * pOtherRect.h - (1 + direction * -2) * (margin + GetRect().h + GetRelativePos().y);
			oldPos = pTransform->GetPosition();
			pTransform->SetPosition(oldPos.x, newPos, oldPos.z);

			if(stopVelocity)
			{
				//Halt y movement
				currVel = pPhysicsComp->GetVelocity();
				pPhysicsComp->SetVelocity(currVel.x, 0.f, currVel.z);
			}
			break;
			
		case Side::Left:
			direction = 1.f;
		case Side::Right:
			//get new x position and transform to it
			newPos = pOtherRect.x + direction * pOtherRect.w - (1 + direction * -2) * (margin + GetRect().w + GetRelativePos().x);
			oldPos = pTransform->GetPosition();
			pTransform->SetPosition(newPos, oldPos.y, oldPos.z);

			if (stopVelocity)
			{
				//Halt x movement
				currVel = pPhysicsComp->GetVelocity();
				pPhysicsComp->SetVelocity(0.f, currVel.y, currVel.z);
			}
			break;
		default:
			break;
		}
	}
};

class PlayerCollisionComponent final : public CharacterCollisionComponent
{
public:
	explicit PlayerCollisionComponent(const std::shared_ptr<Budgety::Texture2DComponent>& pTextureComp) : CharacterCollisionComponent(Budgety::CollisionGroup::Player, pTextureComp) {}
	explicit PlayerCollisionComponent(const glm::vec2& dimensions, const glm::vec2& pivot) : CharacterCollisionComponent(Budgety::CollisionGroup::Player, dimensions, pivot) {}
	explicit PlayerCollisionComponent(int width, int height, const glm::vec2& pivot) : CharacterCollisionComponent(Budgety::CollisionGroup::Player, width, height, pivot) {}
	~PlayerCollisionComponent() = default;

	void OnCollisionEnter(Budgety::Entity* pOtherEntity, Budgety::Side enterSide) override
	{
		using namespace Budgety;

		CharacterCollisionComponent::OnCollisionEnter(pOtherEntity, enterSide);

		auto pOtherCollisionComp = pOtherEntity->GetComponent<CollisionComponent>();
		CollisionGroup otherGroup = pOtherCollisionComp->GetCollisionGroup();

		switch (otherGroup)
		{
		case CollisionGroup::Enemy:
		{
			GetEntity()->GetComponent<CharacterComponent>()->Kill();
		}
		break;
		case CollisionGroup::Bubble:
		{
			auto pEntity = GetEntity();
			auto pCharComp = pEntity->GetComponent<CharacterComponent>();

				if(pCharComp->GetState() == CharacterComponent::State::Airborn)
				{
					if (enterSide == Side::Bottom) //landing on bubble
					{
						if (pCharComp->GetWantsToJump()) //if user inputted jump recently, jump
						{
							pCharComp->Jump(true);
							pOtherEntity->GetScene()->RemoveEntity(pOtherEntity);
						}
					}
				}
		}
		break;
		}
	}
};

class EnemyCollisionComponent final : public CharacterCollisionComponent
{
public:
	explicit EnemyCollisionComponent(const std::shared_ptr<Budgety::Texture2DComponent>& pTextureComp) : CharacterCollisionComponent(Budgety::CollisionGroup::Enemy, pTextureComp) {}
	explicit EnemyCollisionComponent(const glm::vec2& dimensions, const glm::vec2& pivot) : CharacterCollisionComponent(Budgety::CollisionGroup::Enemy, dimensions, pivot) {}
	explicit EnemyCollisionComponent(int width, int height, const glm::vec2& pivot) : CharacterCollisionComponent(Budgety::CollisionGroup::Enemy, width, height, pivot) {}
	~EnemyCollisionComponent() = default;

	void OnCollisionEnter(Budgety::Entity* pOtherEntity, Budgety::Side enterSide) override
	{
		using namespace Budgety;

		CharacterCollisionComponent::OnCollisionEnter(pOtherEntity, enterSide);

		auto pOtherCollisionComp = pOtherEntity->GetComponent<CollisionComponent>();
		CollisionGroup otherGroup = pOtherCollisionComp->GetCollisionGroup();

		switch (otherGroup)
		{
		case CollisionGroup::Bubble:
		{
			auto pCharComp = GetEntity()->GetComponent<CharacterComponent>();
			auto state = pCharComp->GetState();
			if (state == CharacterComponent::State::Grounded || state == CharacterComponent::State::Airborn)
				pCharComp->Kill();
		}
		break;
		}
	}
};