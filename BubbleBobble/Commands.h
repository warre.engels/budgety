#pragma once
#include "Command.h"
#include "Entity.h"
#include "Components.h"
#include "CharacterClasses.h"

class TogglePauseCommand : public Budgety::Command
{
public:
	void Execute(Budgety::Entity* pEntity) const override
	{
		pEntity->GetScene()->TogglePause();
	}
};

class MoveRightCommand : public Budgety::Command
{
public:
	void Execute(Budgety::Entity* pEntity) const override
	{
		if (!pEntity->GetScene()->GetIsPaused())
			pEntity->GetComponent<Budgety::CharacterComponent>()->MoveRight();
	}
};

class MoveLeftCommand : public Budgety::Command
{
public:
	void Execute(Budgety::Entity* pEntity) const override
	{
		if (!pEntity->GetScene()->GetIsPaused())
			pEntity->GetComponent<Budgety::CharacterComponent>()->MoveLeft();
	}
};

class JumpCommand : public Budgety::Command
{
public:
	void Execute(Budgety::Entity* pEntity) const override
	{
		if (!pEntity->GetScene()->GetIsPaused())
			pEntity->GetComponent<Budgety::CharacterComponent>()->Jump();
	}
};

class ShootCommand : public Budgety::Command
{
public:
	void Execute(Budgety::Entity* pEntity) const override
	{
		if (!pEntity->GetScene()->GetIsPaused())
			static_cast<PlayerCharacterComponent*>(pEntity->GetComponent<Budgety::CharacterComponent>().get())->Shoot();
	}
};