#pragma once
#include "Components.h"
#include "Entity.h"
#include "Time.h"
#include <iostream>
#include <deque>
#include "CollisionClasses.h"

class PlayerCharacterComponent final : public Budgety::CharacterComponent
{
public:
	explicit PlayerCharacterComponent(float speed = 100.f, float jumpForce = 270.f) : Budgety::CharacterComponent(speed, jumpForce) {}

	void Update() override
	{
		Budgety::CharacterComponent::Update();

		float elapsedSec = Budgety::Time::GetInstance().GetElapsed();
		
		if(m_ShouldRespawn)
		{
			m_ElapsedRespawnSec += elapsedSec;
			if(m_ElapsedRespawnSec >= 0.2f * 4) //death frametime * nbFrames
			{
				GetEntity()->GetComponent<Budgety::Texture2DComponent>()->SetIsActive(false);
			}
			if(m_ElapsedRespawnSec >= m_MaxRespawnSec)
			{
				m_ElapsedRespawnSec = 0.f;
				m_ShouldRespawn = false;
				Respawn();
			}
		}

		if(m_IsShooting)
		{
			m_ElapsedShootSec += elapsedSec;
			if(m_ElapsedShootSec >= m_MaxShootSec)
			{
				m_ElapsedShootSec = 0.f;
				m_IsShooting = false;

				auto pTexComp = GetEntity()->GetComponent<Budgety::Texture2DComponent>();

				if (pTexComp->GetAnimation() == int(PlayerAnimation::Shootleft))
					pTexComp->SetAnimation(int(PlayerAnimation::Movingleft));

				if (pTexComp->GetAnimation() == int(PlayerAnimation::Shootright))
					pTexComp->SetAnimation(int(PlayerAnimation::Movingright));
			}
		}

		for (auto& bubble : m_Bubbles)
		{
			bubble.accSec += elapsedSec;
		}

		auto it = std::remove_if(m_Bubbles.begin(), m_Bubbles.end(),
			[this](const Bubble& bubble)
			{
				if (bubble.pBubble == nullptr)
					return true;
				if (bubble.accSec >= bubble.maxSec)
				{
					GetEntity()->GetScene()->RemoveEntity(bubble.pBubble);
					return true;
				}
				return false;
			});
		if (it != m_Bubbles.end())
			m_Bubbles.erase(it, m_Bubbles.end());
	}

	void SetState(State state) override
	{
		using namespace Budgety;
		CharacterComponent::SetState(state);

		switch (state)
		{
		case State::Dead:
			GetEntity()->GetComponent<Texture2DComponent>()->SetAnimation(int(PlayerAnimation::Death));
			GetEntity()->GetComponent<Budgety::PhysicsComponent>()->SetVelocity(0, 0, 0);
			break;

		default:
			break;
		}
	}

	void Kill() override
	{
		if (--m_AmountOfLives == 0)
		{
			//notify player killed
		}

		m_ShouldRespawn = true;
		Despawn();
	}

	void Shoot()
	{
		if (GetState() == State::Dead)
			return;

		if (m_IsShooting)
			return;

		auto pEntity = GetEntity();
		auto pTexComp = pEntity->GetComponent<Budgety::Texture2DComponent>();
		bool shootLeft = false;
		
		//animation
		if (pTexComp->GetAnimation() == int(PlayerAnimation::Movingleft))
		{
			pTexComp->SetAnimation(int(PlayerAnimation::Shootleft));
			shootLeft = true;
		}
		else if (pTexComp->GetAnimation() == int(PlayerAnimation::Movingright))
			pTexComp->SetAnimation(int(PlayerAnimation::Shootright));
		else
			return;

		m_IsShooting = true;
		auto pTransform = pEntity->GetComponent<Budgety::TransformComponent>();

		//bubble
#pragma warning(push)
//avoid unnamed objects
#pragma warning (disable:26444)
		auto pBubbleEntity = pEntity->GetScene()->AddEntity(true);
		pBubbleEntity->AddComponent(std::make_shared<Budgety::TransformComponent>(pTransform->Get2DPosition()));
		auto pTex2DComp = pBubbleEntity->AddComponent(std::make_shared<Budgety::Texture2DComponent>("sprites.png", SPRITESIZE, SPRITESIZE, glm::vec2{ 0.5f, 0.5f }, SCALE));
		pTex2DComp->AddAnimation(8, SPRITESIZE, SPRITESIZE, 0.2f, SPRITESIZE * 29, 18);
		pBubbleEntity->AddComponent(std::make_shared<Budgety::PhysicsComponent>(-10.f, -30.f))->AddForce((1 - int(shootLeft) * 2) * 200.f, 0.f, 0.f);
		pBubbleEntity->AddComponent(std::dynamic_pointer_cast<Budgety::CollisionComponent>(std::make_shared<NoCollisionBehaviourComponent>(Budgety::CollisionGroup::Bubble, pTex2DComp)));
#pragma warning(pop)

		if (m_Bubbles.size() == m_MaxAmountOfBubbles)
		{
			GetEntity()->GetScene()->RemoveEntity(m_Bubbles[0].pBubble);
			m_Bubbles.pop_front();
		}
		
		m_Bubbles.push_back(pBubbleEntity.get());
	}

private:
	void Despawn()
	{
		//std::cout << "Despawn\n";

		GetEntity()->GetComponent<Budgety::PhysicsComponent>()->SetIsActive(false);
		GetEntity()->GetComponent<Budgety::CollisionComponent>()->SetIsActive(false);
		
		SetState(State::Dead);
	}
	
	void Respawn()
	{
		//std::cout << "Respawn\n";

		auto pEntity = GetEntity();
		pEntity->GetComponent<Budgety::TransformComponent>()->Set2DPosition({ player1SpawnPosX, playerSpawnPosY });
		pEntity->GetComponent<Budgety::Texture2DComponent>()->SetAnimation(int(PlayerAnimation::Movingright));
		pEntity->GetComponent<Budgety::PhysicsComponent>()->SetVelocity(0, 0, 0);

		pEntity->GetComponent<Budgety::PhysicsComponent>()->SetIsActive(true);
		pEntity->GetComponent<Budgety::CollisionComponent>()->SetIsActive(true);
		pEntity->GetComponent<Budgety::Texture2DComponent>()->SetIsActive(true);
		SetState(State::Airborn);
	}
	
	int m_AmountOfLives = 4;

	float m_MaxRespawnSec = 2.f;
	float m_ElapsedRespawnSec = 0.f;
	bool m_ShouldRespawn = false;

	bool m_IsShooting = false;
	float m_MaxShootSec = 4 * 0.1f; //nbFrames * frameTime
	float m_ElapsedShootSec = 0.f;

	size_t m_MaxAmountOfBubbles = 10;
	std::deque<Bubble> m_Bubbles{};
};
const float Bubble::maxSec = 5.f;

class EnemyCharacterComponent final : public Budgety::CharacterComponent
{
public:
	explicit EnemyCharacterComponent(float speed = 50.f, float jumpForce = 270.f) : Budgety::CharacterComponent(speed, jumpForce) {}

	void SetState(State state) override
	{
		using namespace Budgety;
		CharacterComponent::SetState(state);

		switch (state)
		{
		case State::Bubbled:
			GetEntity()->GetComponent<Texture2DComponent>()->SetAnimation(int(EnemyAnimation::InBubble));
			break;

		case State::Dead:
			GetEntity()->GetComponent<Texture2DComponent>()->SetAnimation(int(EnemyAnimation::Death));
			break;

		default:
			break;
		}
	}
	
	void Kill() override
	{
		if(GetState() == State::Bubbled)
		{
			SetState(State::Dead);
		}
		else
		{
			SetState(State::Bubbled);
		}
	}
};