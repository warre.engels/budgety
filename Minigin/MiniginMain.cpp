#include "MiniginPCH.h"
#include "MiniginMain.h"

#include "SDL.h"
#include "Minigin.h"

#if _DEBUG
#include <vld.h>
#endif

void Budgety::Run(void(*LoadGameFunc)())
{
#if _DEBUG
	try
	{
#endif
		Budgety::Minigin engine;
		engine.Run(LoadGameFunc);
#if _DEBUG
	}
	catch(std::exception& e)
	{
		std::stringstream message{};
		message << "Exception: " << e.what();
		int msgboxID = MessageBox(NULL, message.str().c_str(), "", MB_ICONERROR | MB_OK);
		if (msgboxID == IDOK)
			return;
	}
	catch(...)
	{
		int msgboxID = MessageBox(NULL, "Exception of an undetermined type", "", MB_ICONERROR | MB_OK);
		if (msgboxID == IDOK)
			return;
	}
#endif
}