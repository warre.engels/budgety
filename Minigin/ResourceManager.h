#pragma once
#include "Singleton.h"
#include <unordered_map>
#include <functional>

namespace Budgety
{
	class Texture2D;
	class Font;
	
	class ResourceManager final : public Singleton<ResourceManager>
	{
	public:
		void Init(const std::string& data);
		std::shared_ptr<Texture2D> LoadTexture(const std::string& file);
		std::shared_ptr<Font> LoadFont(const std::string& file, unsigned int size);
	private:
		friend class Singleton<ResourceManager>;
		ResourceManager() = default;

		struct FontSettings
		{
			std::string fileName;
			unsigned int size;

			bool operator==(const FontSettings& other) const
			{
				return fileName == other.fileName && size == other.size;
			}
		};
		struct FontSettingsHash
		{
			size_t operator()(const FontSettings& other) const
			{
				return std::hash<std::string>{}(other.fileName);
			}
		};

		std::string m_DataPath;
		std::unordered_map<std::string, std::shared_ptr<Texture2D>> m_pTextures;
		std::unordered_map<FontSettings, std::shared_ptr<Font>, FontSettingsHash> m_pFonts;
	};
}
