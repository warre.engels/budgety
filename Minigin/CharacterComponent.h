#pragma once
#include "Component.h"
#include "HelperTypes.h"

namespace Budgety
{
	class CharacterComponent : public Component
	{
	public:
		enum class State
		{
			Grounded,
			Airborn,
			Bubbled,
			Dead
		};
		
		explicit CharacterComponent(float speed = 60.f, float jumpForce = 500.f);
		virtual ~CharacterComponent() = default;
		
		virtual void Update() override;
		void Render() const override;

		inline State GetState() const { return m_State; }
		inline float GetSpeed() const { return m_Speed; }
		inline bool GetWantsToJump() const { return m_WantsToJump; }

		virtual void SetState(State state);

		virtual void Kill() = 0;
		
		inline void MoveLeft() { Move(true); }
		inline void MoveRight() { Move(false); }
		void Jump(bool force = false);
		
	private:
		void Move(bool moveLeft);
		
		float m_Speed;
		float m_JumpForce;

		State m_State;

		float m_MaxWantsToJumpSec;
		float m_ElapsedWantsToJumpSec;
		bool m_WantsToJump;
	};
}