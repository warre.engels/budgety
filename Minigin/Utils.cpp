#include "MiniginPCH.h"
#include "Utils.h"
#include "CollisionComponent.h"

std::string utils::to_string_with_precision(const float value, const int precision)
{
	std::ostringstream out;
	out.precision(precision);
	out << std::fixed << value;

	return out.str();
}

bool utils::MouseInRect(const glm::vec2& mousePos, const SDL_Rect& rect)
{
	if (mousePos.x < rect.x || mousePos.x > rect.x + rect.w)
		return false;
	if (mousePos.y < rect.y || mousePos.y > rect.y + rect.h)
		return false;

	return true;
}

bool utils::IsOverlapping(const int expandAmount, const SDL_Rect& rect1, const SDL_Rect& rect2)
{
	//left or right
	if ((rect1.x + rect1.w + expandAmount) < rect2.x || (rect2.x + rect2.w) < rect1.x - expandAmount)
	{
		return false;
	}

	//over or under
	if ((rect1.y + rect1.h + expandAmount) < rect2.y || (rect2.y + rect2.h) < rect1.y - expandAmount)
	{
		return false;
	}

	return true;
}

bool utils::IsOverlapping(const SDL_Rect& rect1, const SDL_Rect& rect2)
{
	//left or right
	if ((rect1.x + rect1.w) < rect2.x || (rect2.x + rect2.w) < rect1.x)
	{
		return false;
	}

	//over or under
	if ((rect1.y + rect1.h) < rect2.y || (rect2.y + rect2.h) < rect1.y)
	{
		return false;
	}

	return true;
}

Budgety::Side utils::GetOppositeSide(const Budgety::Side side)
{
	switch (side)
	{
	case Budgety::Side::Top:		return Budgety::Side::Bottom;
	case Budgety::Side::Bottom:		return Budgety::Side::Top;
	case Budgety::Side::Left:		return Budgety::Side::Right;
	case Budgety::Side::Right:		return Budgety::Side::Left;
	}
	return Budgety::Side();
}