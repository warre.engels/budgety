#pragma once
#include "HelperTypes.h"

struct SDL_Window;

namespace Budgety
{
	constexpr float secPerFrame =
#ifdef FRAMERATECAP
		1.f / FRAMERATECAP;
#else
		0.f;
#endif
	
	class Minigin final
	{
	public:
		Minigin() = default;
		~Minigin() = default;

		Minigin(const Minigin& other) = delete;
		Minigin(Minigin&& other) = delete;
		Minigin& operator=(const Minigin& other) = delete;
		Minigin& operator=(Minigin&& other) = delete;
		
		void Run(void(*LoadGameFunc)());
		
	private:
		void Initialize();
		void Cleanup();
		
		SDL_Window* m_pWindow = nullptr;
	};
}