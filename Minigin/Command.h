#pragma once

namespace Budgety
{
	class Entity;
	
	class Command
	{
	public:
		Command() = default;
		virtual ~Command() = default;
		
		Command(const Command& other) = default;
		Command(Command&& other) = default;
		Command& operator=(const Command& other) = default;
		Command& operator=(Command&& other) = default;

		virtual void Execute(Entity* pEntity) const = 0;
	};
}