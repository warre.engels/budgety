#pragma once
#include "Singleton.h"
#include "Minigin.h"
#include "Scene.h"
#include <chrono>

struct SDL_Window;

namespace Budgety
{
	class Time final : public Singleton<Time>
	{
	public:
		std::chrono::steady_clock::time_point GetCurrent() const;
		void PrintDuration(const std::chrono::steady_clock::time_point& start, const std::chrono::steady_clock::time_point& end, const std::string& text) const;
		float GetElapsed() const;

	private:
		friend class Singleton<Time>;
		Time();
		
		friend void Minigin::Run(void(*LoadGameFunc)());
		
		void SetElapsed(float elapsedSec);
		
		float m_ElapsedSec;
	};
}