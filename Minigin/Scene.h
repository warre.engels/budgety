#pragma once
#include "SceneManager.h"
#include "CollisionComponent.h"

namespace Budgety
{
	class Entity;
	
	class Scene final
	{
	public:
		~Scene();
		Scene(const Scene& other) = delete;
		Scene(Scene&& other) = delete;
		Scene& operator=(const Scene& other) = delete;
		Scene& operator=(Scene&& other) = delete;

		void Update();
		void Render() const;
		
		std::shared_ptr<Entity> AddEntity(std::shared_ptr<Entity> pObject, bool renderLast = false);
		//makes an Entity, adds it to the scene and returns it
		[[nodiscard]] std::shared_ptr<Entity> AddEntity(bool renderLast = false);
		void RemoveEntity(Entity* pEntity);

		inline const std::string& GetName() const { return m_Name; }
		inline const bool GetIsPaused() const { return m_IsPaused; }

		void AddCollisionComponent(std::shared_ptr<CollisionComponent> pCollComp);

		void TogglePause();
		void Resume();
		void Pause();
		
	private:
		friend Scene& SceneManager::CreateScene(std::string name);
		explicit Scene(std::string name);

		void CheckCollision(CollisionComponent* pComp1, CollisionComponent* pComp2) const;

		std::string m_Name;
		std::vector<std::shared_ptr<Entity>> m_pEntities{};
		std::vector<Entity*> m_pRenderLastEntities{};
		std::vector<Entity*> m_pRemoveEntityList{};
		std::vector<std::pair<std::weak_ptr<CollisionComponent>, CollisionComponent*>> m_pCollisionComponents{};
		
		bool m_IsPaused;
	};
}
