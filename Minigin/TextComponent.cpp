#include "MiniginPCH.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "Font.h"
#include "Texture2D.h"
#include "TextComponent.h"
#include "Entity.h"
#include "TransformComponent.h"

Budgety::TextComponent::TextComponent(const std::string& text, const std::shared_ptr<Font>& pFont, glm::vec2 pivot)
	: Component()
	, m_NeedsUpdate{ true }
	, m_Text{ text }
	, m_pFont{ pFont }
	, m_pTexture{ nullptr }
	, m_Color{ 255, 255, 255 }
	, m_Pivot{ std::move(pivot) }
{
	
}

Budgety::TextComponent::TextComponent(const std::string& text, const std::string& fontPath, unsigned int fontSize, glm::vec2 pivot)
	: TextComponent(text, nullptr, pivot)
{
	std::shared_ptr<Font> pFont = ResourceManager::GetInstance().LoadFont(fontPath, fontSize);
	m_pFont = pFont;
}

void Budgety::TextComponent::Update()
{
	if (m_NeedsUpdate)
	{
		const auto pSurf = TTF_RenderText_Blended(m_pFont->GetFont(), m_Text.c_str(), m_Color);
		if (pSurf == nullptr)
		{
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		}
		auto pTexture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), pSurf);
		if (pTexture == nullptr)
		{
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(pSurf);
		m_pTexture = std::make_shared<Texture2D>(pTexture);
		m_NeedsUpdate = false;
	}
}

void Budgety::TextComponent::Render() const
{
	if (m_pTexture == nullptr)
		return;

	if (!GetEntity()->HasComponent<TransformComponent>())
		throw std::exception("TextComponent: TransformComponent not found");
	
	auto pos = GetEntity()->GetComponent<TransformComponent>()->Get2DPosition();
	auto dimensions = m_pTexture->GetDimensions();
	pos -= (m_Pivot * dimensions);
	SDL_Rect rect;
	rect.x = int(pos.x);
	rect.y = int(pos.y);
	rect.w = int(dimensions.x);
	rect.h = int(dimensions.y);
	Renderer::GetInstance().RenderTexture(*m_pTexture, rect);
}

void Budgety::TextComponent::SetText(std::string text)
{
	m_Text = std::move(text);
	m_NeedsUpdate = true;
}

void Budgety::TextComponent::SetColor(SDL_Color color)
{
	m_Color = std::move(color);
	m_NeedsUpdate = true;
}