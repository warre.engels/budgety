#include "MiniginPCH.h"
#include "FPSComponent.h"
#include "Entity.h"
#include "TextComponent.h"

#define REFRESH_TIME 1.f

Budgety::FPSComponent::FPSComponent()
	: Component()
	, m_FPS{}
	, m_LastRefeshTime{ REFRESH_TIME - 0.1f } //first update after 0.1 sec
{
	
}

void Budgety::FPSComponent::Update()
{
	if (!GetEntity()->HasComponent<TextComponent>())
		throw std::exception("FPSComponent: no TextComponent added");

	float elapsedSec = Time::GetInstance().GetElapsed();

	m_LastRefeshTime += elapsedSec;
	if(m_LastRefeshTime >= REFRESH_TIME)
	{
		m_FPS = int(1.f / elapsedSec);
		GetEntity()->GetComponent<TextComponent>()->SetText(std::to_string(m_FPS));
		m_LastRefeshTime = 0.f;
	}
}

void Budgety::FPSComponent::Render() const
{
	
}

std::shared_ptr<Budgety::TextComponent> Budgety::FPSComponent::AddTextComponent(std::shared_ptr<Budgety::TextComponent> pText)
{
	return GetEntity()->AddComponent(std::move(pText));
}