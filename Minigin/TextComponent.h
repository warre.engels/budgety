#pragma once
#pragma warning(push)
//nonstandard extension used : nameless struct/union
#pragma warning (disable : 4201)
#include <glm/vec2.hpp>
#pragma warning(pop)

#include "Component.h"
#include <string>
#include <SDL.h>
#include <SDL_ttf.h>

namespace Budgety
{
	class Font;
	class Texture2D;
	
	class TextComponent final : public Component
	{
	public:
		explicit TextComponent(const std::string& text, const std::shared_ptr<Font>& pFont, glm::vec2 pivot = { 0.f, 0.f });
		explicit TextComponent(const std::string& text, const std::string& fontPath, unsigned int fontSize, glm::vec2 pivot = { 0.f, 0.f });

		void Update() override;
		void Render() const override;
		
		inline const glm::vec2& GetPivot() const { return m_Pivot; }

		inline void SetPivot(glm::vec2 pivot) { m_Pivot = std::move(pivot); }
		void SetText(std::string text);
		inline void SetColor(Uint8 r, Uint8 g, Uint8 b) { SetColor(SDL_Color{ r, g, b }); }
		void SetColor(SDL_Color color);
		
	private:
		bool m_NeedsUpdate;
		std::string m_Text;
		std::shared_ptr<Font> m_pFont;
		std::shared_ptr<Texture2D> m_pTexture;
		SDL_Color m_Color;
		glm::vec2 m_Pivot;
	};
}