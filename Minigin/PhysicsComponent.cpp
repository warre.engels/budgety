#include "MiniginPCH.h"
#include "PhysicsComponent.h"
#include "Components.h"
#include "Entity.h"

Budgety::PhysicsComponent::PhysicsComponent(float mass, float terminalVelocity, float g, float airResistance)
	: Component()
	, m_Mass{ mass }
	, m_TerminalVel{ terminalVelocity }
	, m_G{ g }
	, m_AirResistance{ airResistance }
	, m_IsGravityEnabled{ true }
	, m_Velocity{ 0.f }
{
	
}

void Budgety::PhysicsComponent::Update()
{
	float elapsedSec = Time::GetInstance().GetElapsed();

	if(m_IsGravityEnabled)
	{	//apply gravity
		if(m_TerminalVel > 0.f)
		{	//positive mass and terminal velocity (fall downwards)
			if (m_Velocity.y < m_TerminalVel)
			{
				m_Velocity.y += m_Mass * m_G * elapsedSec;
			}
			else if (m_Velocity.y > m_TerminalVel)
			{
				m_Velocity.y = m_TerminalVel;
			}
		}
		else
		{	//negative mass and terminal velocity (fall upwards)
			if (m_Velocity.y > m_TerminalVel)
			{
				m_Velocity.y += m_Mass * m_G * elapsedSec;
			}
			else if (m_Velocity.y < m_TerminalVel)
			{
				m_Velocity.y = m_TerminalVel;
			}
		}
	}

	//air resistance
	if (m_Velocity.x < 0.5f)
		m_Velocity.x += m_AirResistance * elapsedSec;
	else if (m_Velocity.x > 0.5f)
		m_Velocity.x -= m_AirResistance * elapsedSec;
	else
		m_Velocity.x = 0.f;
	
	//move object
	auto pTransform = GetEntity()->GetComponent<TransformComponent>();
	pTransform->SetPosition(pTransform->GetPosition() + m_Velocity * elapsedSec);
}

void Budgety::PhysicsComponent::Render() const
{
	
}