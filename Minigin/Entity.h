#pragma once
#include <vector>
#include <array>
#include <bitset>
#include "Component.h"
#include <sstream>

using ComponentTypeID = size_t;

inline ComponentTypeID GetComponentTypeID()
{
	//starts at 0, every time this function is called it sends back an incremented typeID
	static ComponentTypeID lastID{ 0 };
	return lastID++;
}

template <typename T>
inline ComponentTypeID GetComponentTypeID() noexcept
{
	//different function for every type -> GetComponentTypeID() called once per type
	//thus only 1 typeID per type is generated
	static ComponentTypeID typeID{ GetComponentTypeID() };
	return typeID;
}

constexpr size_t maxNrComponents{ 32 };

namespace Budgety
{
	class Scene;
	
	class Entity final
	{
	public:
		Entity() = default;
		~Entity() = default;

		Entity(const Entity& other) = delete;
		Entity(Entity&& other) = delete;
		Entity& operator=(const Entity& other) = delete;
		Entity& operator=(Entity&& other) = delete;
		
		void Update()
		{
			if (!m_IsActive)
				return;
			
			for (auto& pComponent : m_pComponents)
				if(pComponent->GetIsActive())
					pComponent->Update();
		}
		void Render() const
		{
			if (!m_IsActive)
				return;

			for (const auto& pComponent : m_pComponents)
				if (pComponent->GetIsActive())
					pComponent->Render();
		}

		template<typename T>
		bool HasComponent() const;
		template<typename T>
		std::shared_ptr<T> GetComponent() const;
		template<typename T>
		std::shared_ptr<T> AddComponent(const std::shared_ptr<T>& pComponent);

		void SetScene(Scene* pScene) { m_pScene = pScene; }
		Scene* GetScene() const { return m_pScene; }

		inline void SetIsActive(bool isActive) { m_IsActive = isActive; }
		inline bool GetIsActive() const { return m_IsActive; }
		
	private:
		std::vector<std::shared_ptr<Component>> m_pComponents = {};
		
		std::array<std::weak_ptr<Component>, maxNrComponents> m_ComponentArray = {};
		std::bitset<maxNrComponents> m_ComponentBitset = {};

		Scene* m_pScene = nullptr;

		bool m_IsActive = true;
	};

	template <typename T>
	bool Entity::HasComponent() const
	{
		return m_ComponentBitset[GetComponentTypeID<T>()];
	}

	template <typename T>
	std::shared_ptr<T> Entity::GetComponent() const
	{
		ComponentTypeID id = GetComponentTypeID<T>();
		
		if (!m_ComponentBitset[id])
		{
			const char* type = typeid(T).name() + sizeof("classBudgety::");
			unsigned int nrOfComponents{ 0 };
			std::stringstream text{ "" };

			for (size_t i{ 0 }; i < maxNrComponents; i++)
				if (m_ComponentBitset[i])
					nrOfComponents++;

			text << "Entity with " << nrOfComponents << " Components does not have a " << type << ", yet GetComponent<" << type << ">() is called";
			throw std::exception(text.str().c_str());
		}
		
		return std::dynamic_pointer_cast<T>(m_ComponentArray[id].lock());
	}

	template <typename T>
	std::shared_ptr<T> Entity::AddComponent(const std::shared_ptr<T>& pComponent)
	{
		pComponent->SetEntity(this);
		pComponent->Init();
		m_pComponents.push_back(pComponent);

		ComponentTypeID id = GetComponentTypeID<T>();
		m_ComponentArray[id] = pComponent;
		m_ComponentBitset[id] = true;

		return pComponent;
	}
}