#include "MiniginPCH.h"
#include "ResourceManager.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "Renderer.h"
#include "Texture2D.h"
#include "Font.h"

void Budgety::ResourceManager::Init(const std::string& dataPath)
{
	m_DataPath = dataPath;

	// load support for png and jpg, this takes a while!

	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
	{
		throw std::runtime_error(std::string("Failed to load support for png's: ") + SDL_GetError());
	}

	if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG)
	{
		throw std::runtime_error(std::string("Failed to load support for jpg's: ") + SDL_GetError());
	}

	if (TTF_Init() != 0)
	{
		throw std::runtime_error(std::string("Failed to load support for fonts: ") + SDL_GetError());
	}
}

std::shared_ptr<Budgety::Texture2D> Budgety::ResourceManager::LoadTexture(const std::string& file)
{
	const auto fullPath = m_DataPath + file;
	std::shared_ptr<Texture2D> pTexture;

	const auto it = m_pTextures.find(file);
	
	if(it != m_pTextures.cend())
	{	//already an existing texture
		pTexture = it->second;
	}
	else
	{	//make a new texture
		SDL_Texture* pSDLTexture = IMG_LoadTexture(Renderer::GetInstance().GetSDLRenderer(), fullPath.c_str());
		if (pSDLTexture == nullptr)
		{
			throw std::runtime_error(std::string("Failed to load texture: ") + SDL_GetError());
		}
		pTexture = std::make_shared<Texture2D>(pSDLTexture);

		m_pTextures.emplace(file, pTexture);
	}

	return pTexture;
}

std::shared_ptr<Budgety::Font> Budgety::ResourceManager::LoadFont(const std::string& file, unsigned int size)
{
	const auto fullPath = m_DataPath + file;
	FontSettings fontSettings{ file, size };
	std::shared_ptr<Font> pFont;

	const auto it = m_pFonts.find(fontSettings);

	if (it != m_pFonts.cend())
	{	//already an existing font
		pFont = it->second;
	}
	else
	{	//make a new font
		pFont = std::make_shared<Font>(fullPath, size);

		m_pFonts.emplace(fontSettings, pFont);
	}

	return pFont;
}
