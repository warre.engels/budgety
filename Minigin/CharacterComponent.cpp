#include "MiniginPCH.h"
#include "CharacterComponent.h"
#include "Components.h"
#include "Entity.h"

constexpr int bottomOfLevel = int(levelDisplacementY + LEVELHEIGHT * BLOCKSIZE * SCALE + SPRITESIZE);
constexpr int topOfLevel = int(levelDisplacementY - SPRITESIZE);

Budgety::CharacterComponent::CharacterComponent(float speed, float jumpForce)
	: Component()
	, m_Speed{ speed }
	, m_JumpForce{ jumpForce }
	, m_State{ State::Airborn }
	, m_WantsToJump{ false }
	, m_MaxWantsToJumpSec{ 0.5f }
	, m_ElapsedWantsToJumpSec{ 0.f }
{
	
}

void Budgety::CharacterComponent::Update()
{
	//handle jumptimer
	if (m_WantsToJump)
	{
		m_ElapsedWantsToJumpSec += Time::GetInstance().GetElapsed();
		if (m_ElapsedWantsToJumpSec >= m_MaxWantsToJumpSec)
		{
			m_ElapsedWantsToJumpSec = 0.f;
			m_WantsToJump = false;
		}
	}
	
	//handle out of level from bot or top
	auto pTransform = GetEntity()->GetComponent<TransformComponent>();
	auto pos = pTransform->Get2DPosition();

	bool updatePosition = false;
	if (pos.y > bottomOfLevel)
	{
		pos.y = topOfLevel;
		updatePosition = true;
	}
	if (pos.y < topOfLevel)
	{
		pos.y = bottomOfLevel;
		updatePosition = true;
	}

	if (updatePosition)
		pTransform->Set2DPosition(pos);
}

void Budgety::CharacterComponent::Render() const
{
	
}

void Budgety::CharacterComponent::SetState(State state)
{
	m_State = state;

	switch (m_State)
	{
	case State::Grounded:
		GetEntity()->GetComponent<PhysicsComponent>()->DisableGravity();
		break;
		
	case State::Airborn:
		GetEntity()->GetComponent<PhysicsComponent>()->EnableGravity();
		break;
		
	default:
		break;
	}
}

void Budgety::CharacterComponent::Move(bool moveLeft)
{
	if (m_State == State::Dead)
		return;
	
	float elapsedSec = Time::GetInstance().GetElapsed();
	auto pEntity = GetEntity();
	auto pTransform = pEntity->GetComponent<TransformComponent>();
	float speed = moveLeft ? -m_Speed : m_Speed;

	auto currPos = pTransform->GetPosition();
	pTransform->SetPosition(currPos.x + speed * elapsedSec, currPos.y, currPos.z);

	auto pTexComp = GetEntity()->GetComponent<Texture2DComponent>();
	if (moveLeft) //note: moveleft and right are the same for every 'character' (this doesn't have to be a player)
	{
		if (pTexComp->GetAnimation() == int(PlayerAnimation::Movingright))
			pTexComp->SetAnimation(int(PlayerAnimation::Movingleft));
	}
	else
	{
		if (pTexComp->GetAnimation() == int(PlayerAnimation::Movingleft))
			pTexComp->SetAnimation(int(PlayerAnimation::Movingright));
	}
}

void Budgety::CharacterComponent::Jump(bool force)
{
	if (m_State == State::Dead)
		return;
	
	if (m_State == State::Grounded || force)
	{
		auto pEntity = GetEntity();
	
		if (!pEntity->HasComponent<PhysicsComponent>())
			throw std::exception("CharacterComponent::Jump: pEntity doesn't have a PhysicsComponent");

		auto pPhysics = pEntity->GetComponent<PhysicsComponent>();

		auto currVel = pPhysics->GetVelocity();
		pPhysics->SetVelocity(currVel.x, -m_JumpForce, currVel.z);

		SetState(State::Airborn);
	}
	else if(m_State == State::Airborn)
	{
		m_WantsToJump = true;
	}
}