#include "MiniginPCH.h"
#include "BinaryReader.h"
#include <assert.h>

std::array<Budgety::LevelData, AMOUNT_OF_LEVELS> Budgety::BinaryReader::ReadLevels(const std::string& fileName)
{
	std::ifstream inStream{};
	std::array<LevelData, AMOUNT_OF_LEVELS> levelDatas{};

	inStream.open(fileName, std::ios::binary);

	if (inStream.is_open())
	{	//read binary file
		unsigned char data;
		unsigned char mask;
		glm::vec2 pos;
		const unsigned int levelSize = LEVELWIDTH * LEVELHEIGHT;

		//for each level
		for(size_t i{}; i < AMOUNT_OF_LEVELS; i++)
		{
			for(int h{}; h < LEVELHEIGHT; h++)
			{
				for (int w{}; w < LEVELWIDTH; w += 8)
				{
					//each byte contains 8 blocks
					Read(data, inStream);

					mask = 0b10000000;
					for (int blockIdx{ 0 }; blockIdx < 8; blockIdx++)
					{
						//is there a block in this position
						if (mask & data)
						{
							pos.x = (w + blockIdx) * (BLOCKSIZE * SCALE);
							pos.y = h * (BLOCKSIZE * SCALE);
							levelDatas[i].push_back(pos);
						}

						mask >>= 1;
					}
				}
			}
		}

		for (LevelData& levelData : levelDatas)
		{
			for (glm::vec2& blockTransforms : levelData)
			{
				blockTransforms.x += levelDisplacementX;
				blockTransforms.y += levelDisplacementY;
			}
		}
	}
	else
		throw std::exception((fileName + " : cannot be opened to read\n").c_str());

	inStream.close();

	return levelDatas;
}

std::array< std::vector<Budgety::EnemyData>, AMOUNT_OF_LEVELS> Budgety::BinaryReader::ReadEnemies(const std::string& fileName)
{
	std::ifstream inStream{};
	std::array<std::vector<EnemyData>, AMOUNT_OF_LEVELS> allLevelEnemyDatas{};

	inStream.open(fileName, std::ios::binary);

	if (inStream.is_open())
	{	//read binary file
		unsigned char data;
		bool isLevelRead;
		EnemyData enemyData;

		//for each level
		for (size_t i{}; i < AMOUNT_OF_LEVELS; i++)
		{
			isLevelRead = false;
			while(!isLevelRead)
			{
				//byte1
				Read(data, inStream);
				if(data == 0b00000000) //go to next level
				{
					isLevelRead = true;
					continue;
				}

				unsigned char enemyType = data & 0b00000111;
				switch (enemyType)
				{
				case 0b00000000:
					enemyData.type = EnemyType::ZenChan;
					break;
				case 0b00000110:
					enemyData.type = EnemyType::Maita;
					break;
				default: //all unknown enemy types become EnemyType::Maita
					enemyData.type = EnemyType::Maita;
					break;
				}

				unsigned char col = data & 0b11111000;
				col >>= 3;

				//byte2
				Read(data, inStream);
				unsigned char row = data & 0b11111000;
				row >>= 3;

				enemyData.pos.x = col * (BLOCKSIZE * SCALE) + SPRITESIZE;
				enemyData.pos.y = row * (BLOCKSIZE * SCALE) + SPRITESIZE;

				//byte3
				Read(data, inStream);
				
				allLevelEnemyDatas[i].push_back(enemyData);
			}
		}

		for (std::vector<EnemyData>& enemyDatas : allLevelEnemyDatas)
		{
			for(EnemyData& enemy : enemyDatas)
			{
				enemy.pos.x += levelDisplacementX;
				enemy.pos.y += levelDisplacementY;
			}
		}
	}
	else
		throw std::exception((fileName + " : cannot be opened to read\n").c_str());

	inStream.close();

	return allLevelEnemyDatas;
}

void Budgety::BinaryReader::Read(std::string& data, std::ifstream& in)
{
	//get size of string
	uint32_t stringSize{};
	in.read((char*)&stringSize, sizeof(stringSize));

	//get string
	char* tempCString{ new char[stringSize] };
	in.read(tempCString, stringSize);

	//store string
	data = "";
	data.append(tempCString, stringSize);

	//cleanup
	delete[] tempCString;
}