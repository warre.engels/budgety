#include "MiniginPCH.h"
#include "Scene.h"
#include "Minigin.h"
#include "Entity.h"
#include "Components.h"
#include <algorithm>
#include "Utils.h"

Budgety::Scene::Scene(std::string name)
	: m_Name{ std::move(name) }
	, m_IsPaused{ false }
{}

Budgety::Scene::~Scene()
{
	
}

std::shared_ptr<Budgety::Entity> Budgety::Scene::AddEntity(std::shared_ptr<Entity> pObject, bool renderLast)
{
	if (renderLast)
		m_pRenderLastEntities.push_back(pObject.get());

	m_pEntities.push_back(pObject);
	pObject->SetScene(this);
	return pObject;
}

std::shared_ptr<Budgety::Entity> Budgety::Scene::AddEntity(bool renderLast)
{
	return AddEntity(std::make_shared<Entity>(), renderLast);
}

void Budgety::Scene::RemoveEntity(Entity* pEntity)
{
	m_pRemoveEntityList.push_back(pEntity);
}

void Budgety::Scene::AddCollisionComponent(std::shared_ptr<CollisionComponent> pCollComp)
{
	m_pCollisionComponents.push_back(std::make_pair(pCollComp, pCollComp.get()));
}

void Budgety::Scene::TogglePause()
{
	if (m_IsPaused)
		Resume();
	else
		Pause();
}

void Budgety::Scene::Resume()
{
	m_IsPaused = false;
}

void Budgety::Scene::Pause()
{
	m_IsPaused = true;
}

void Budgety::Scene::Update()
{
	if (m_IsPaused)
		return;

	for (auto& pEntity : m_pEntities)
	{
		pEntity->Update();
	}

	//remove entities
	if (m_pRemoveEntityList.size() != 0)
	{
		auto it1 = std::remove_if(m_pEntities.begin(), m_pEntities.end(),
			[this](const std::shared_ptr<Entity>& pEntity)
			{
				bool remove = false;
				for (auto pToRemoveEntity : m_pRemoveEntityList)
					if (pEntity.get() == pToRemoveEntity)
						remove = true;
				return remove;
			});
		if (it1 != m_pEntities.end())
			m_pEntities.erase(it1, m_pEntities.end());
		
		auto it2 = std::remove_if(m_pRenderLastEntities.begin(), m_pRenderLastEntities.end(),
			[this](const Entity* pEntity)
			{
				if (pEntity == nullptr)
					return true;
			
				bool remove = false;
				for (auto pToRemoveEntity : m_pRemoveEntityList)
					if (pEntity == pToRemoveEntity)
						remove = true;
				return remove;
			});
		if (it2 != m_pRenderLastEntities.end())
			m_pRenderLastEntities.erase(it2, m_pRenderLastEntities.end());
		
		auto it3 = std::remove_if(m_pCollisionComponents.begin(), m_pCollisionComponents.end(),
			[this](const std::pair<std::weak_ptr<CollisionComponent>, CollisionComponent*>& pCollComp)
			{
				return pCollComp.first.expired();
			});
		if (it3 != m_pCollisionComponents.end())
			m_pCollisionComponents.erase(it3, m_pCollisionComponents.end());

		m_pRemoveEntityList.clear();
	}

	//Collision
	size_t size = m_pCollisionComponents.size();
	CollisionComponent *pCollComp1, *pCollComp2;
	for (size_t i{ 0 }; i < size; i++)
	{
		if (m_pCollisionComponents[i].first.expired())
			continue;
		pCollComp1 = m_pCollisionComponents[i].second;
		for (size_t j{ i + 1 }; j < size; j++)
		{	//check each component once with every other component
			//only check collision if they are not in the same group
			if (m_pCollisionComponents[j].first.expired())
				continue;
			pCollComp2 = m_pCollisionComponents[j].second;
			if (pCollComp1->GetIsActive() && pCollComp2->GetIsActive())
			{
				if (pCollComp1->GetCollisionGroup() != pCollComp2->GetCollisionGroup())
					CheckCollision(pCollComp1, pCollComp2);
				//add:
				//else if (m_pCollisionGroups[i] == CollisionGroup::desiredGroup)
				//	CheckCollision(m_pCollisionComponents[i], m_pCollisionComponents[j]);
				//to give group collision with one another
			}
			
		}
	}
}

void Budgety::Scene::Render() const
{
	for (const auto& pEntity : m_pEntities)
	{
		//if doesn't belong to the m_pRenderLastEntities, render here
		if(std::find_if(m_pRenderLastEntities.cbegin(), m_pRenderLastEntities.cend(),
			[&pEntity](const Entity* pLaterRenderEntity)
			{
               return pEntity.get() == pLaterRenderEntity;
			}) == m_pRenderLastEntities.cend())
		{
			pEntity->Render();
		}
	}

	for (const Entity* pEntity : m_pRenderLastEntities)
		if (pEntity)
			pEntity->Render();
}

void Budgety::Scene::CheckCollision(CollisionComponent* pComp1, CollisionComponent* pComp2) const
{
	const SDL_Rect& rect1 = pComp1->GetRect();
	const SDL_Rect& rect2 = pComp2->GetRect();
	
	Side side;
	Entity* pEntity1 = pComp1->GetEntity();
	Entity* pEntity2 = pComp2->GetEntity();

	//already colliding, check if it exited
	if (pComp1->IsColliding(pEntity2))
	{
		//if not colliding anymore
		if (!utils::IsOverlapping(rect1, rect2))
		{
			pComp1->OnCollisionExit(pEntity2);
			pComp2->OnCollisionExit(pEntity1);
		}
	}
	//not yet colliding, check if they start colliding
	else
	{
		//check if the objects are at all close to each other
		if (utils::IsOverlapping(5, rect1, rect2))
		{
			//if colliding
			if (utils::IsOverlapping(rect1, rect2))
			{
				//check on which side they enter each other
				{
					std::array<int, 4> overlapAmounts{};
				
					overlapAmounts[(unsigned int)Side::Left] = abs(rect2.x + rect2.w - rect1.x);
					overlapAmounts[(unsigned int)Side::Right] = abs(rect1.x + rect1.w - rect2.x);
					overlapAmounts[(unsigned int)Side::Top] = abs(rect2.y + rect2.h - rect1.y);
					overlapAmounts[(unsigned int)Side::Bottom] = abs(rect1.y + rect1.h - rect2.y);

					float speed = -1;
					if (pEntity1->HasComponent<CharacterComponent>())
						speed = pEntity1->GetComponent<CharacterComponent>()->GetSpeed();
					else if (pEntity2->HasComponent<CharacterComponent>())
						speed = pEntity2->GetComponent<CharacterComponent>()->GetSpeed();

					//prevent standing in the air while falling next to a wall
					if (speed != -1)
					{
						int positionInWall = int(speed * Time::GetInstance().GetElapsed());
						if (overlapAmounts[(unsigned int)Side::Left] == positionInWall)
							side = Side::Left;
						else if (overlapAmounts[(unsigned int)Side::Right] == positionInWall)
							side = Side::Right;
						else
						{
							//not running into the wall, calculate side
							auto it = std::min_element(overlapAmounts.cbegin(), overlapAmounts.cend());
							side = Side(it - overlapAmounts.cbegin());
						}
					}
					else
					{
						//calculate side
						auto it = std::min_element(overlapAmounts.cbegin(), overlapAmounts.cend());
						side = Side(it - overlapAmounts.cbegin());
					}
				}
			
				pComp1->OnCollisionEnter(pEntity2, side);
				pComp2->OnCollisionEnter(pEntity1, utils::GetOppositeSide(side));
			}
		}
	}
}