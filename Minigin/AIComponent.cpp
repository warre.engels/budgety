#include "MiniginPCH.h"
#include "AIComponent.h"
#include "Components.h"
#include "Entity.h"
#include "HelperTypes.h"
#pragma warning(push)
//nonstandard extension used : nameless struct/union
#pragma warning (disable : 4201)
#pragma warning (disable : 26812)
#pragma warning (disable : 26495)
#include <glm/geometric.hpp>
#pragma warning(pop)

Budgety::AIComponent::AIComponent()
	: Component()
	, m_pTargets{}
{
	
}

Budgety::AIComponent::AIComponent(std::vector<const Entity*> pTargets)
	: AIComponent()
{
	m_pTargets = std::move(pTargets);
}

void Budgety::AIComponent::Update()
{
	if (m_pTargets.size() == 0)
		return;
	
	auto pEntity = GetEntity();
	auto pTransform = pEntity->GetComponent<TransformComponent>();
	auto pCharacterComp = pEntity->GetComponent<CharacterComponent>();

	if (pCharacterComp->GetState() == CharacterComponent::State::Bubbled || pCharacterComp->GetState() == CharacterComponent::State::Dead)
		return;

	glm::vec2 pos = pTransform->Get2DPosition();
	std::vector<glm::vec2> vectorsToTargets{};
	
	for (const Entity* pTarget : m_pTargets)
		if (pTarget)
			if (pTarget->GetComponent<CharacterComponent>()->GetState() != CharacterComponent::State::Dead)
				vectorsToTargets.push_back(pTarget->GetComponent<TransformComponent>()->Get2DPosition() - pos);

	if (vectorsToTargets.size() == 0)
		return;
	
	auto it = std::min_element(vectorsToTargets.cbegin(), vectorsToTargets.cend(),
		[](const glm::vec2& lhs, const glm::vec2& rhs)
		{
			return glm::length(lhs) < glm::length(rhs);
		});

	if (it->x > SPRITESIZE / 2)
		pCharacterComp->MoveRight();
	else if (it->x < -SPRITESIZE / 2)
		pCharacterComp->MoveLeft();

	if (it->y + SPRITESIZE < 0) //if target is above this (y axis points down)
		if (it->x < SPRITESIZE * 3) //if close to character
			pCharacterComp->Jump();
}

void Budgety::AIComponent::Render() const
{
	
}