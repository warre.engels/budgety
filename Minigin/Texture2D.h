#pragma once
#pragma warning(push)
//nonstandard extension used : nameless struct/union
#pragma warning (disable : 4201)
#pragma warning (disable : 26812)
#pragma warning (disable : 26495)
#include <glm/vec2.hpp>
#pragma warning(pop)

struct SDL_Texture;

namespace Budgety
{
	/**
	 * Simple RAII wrapper for an SDL_Texture
	 */
	class Texture2D
	{
	public:
		explicit Texture2D(SDL_Texture* pTexture);
		~Texture2D();

		Texture2D(const Texture2D &) = delete;
		Texture2D(Texture2D &&) = delete;
		Texture2D & operator= (const Texture2D &) = delete;
		Texture2D & operator= (const Texture2D &&) = delete;

		SDL_Texture* GetSDLTexture() const;
		glm::vec2 GetDimensions() const;
		
	private:
		SDL_Texture* m_pTexture;
		int m_Width, m_Height;
	};
}
