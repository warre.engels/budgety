#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include "HelperTypes.h"

namespace Budgety
{
	typedef std::vector<glm::vec2> LevelData;

	struct EnemyData
	{
		EnemyData() = default;
		EnemyData(EnemyType type, glm::vec2 pos) : type{ type }, pos{ pos } {}
		
		EnemyType type{};
		glm::vec2 pos{};
	};

	class BinaryReader
	{
	public:
        [[nodiscard]] static std::array<LevelData, AMOUNT_OF_LEVELS> ReadLevels(const std::string& fileName);
		[[nodiscard]] static std::array<std::vector<EnemyData>, AMOUNT_OF_LEVELS> ReadEnemies(const std::string& fileName);

	private:
        template <typename T>
		static inline void Read(T& data, std::ifstream& in);
		static void Read(std::string& data, std::ifstream& in);
	};

	template <typename T>
	inline void BinaryReader::Read(T& data, std::ifstream& in)
	{
		if (!std::is_pod<T>())
			throw std::exception("Invalid data to read: can only read strings or pods\n");

		in.read((char*)&data, sizeof(data));
	}
}