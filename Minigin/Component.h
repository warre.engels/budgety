#pragma once
#pragma warning(push)
//nonstandard extension used : nameless struct/union
#pragma warning (disable : 4201)
#pragma warning (disable : 26812)
#pragma warning (disable : 26495)
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#pragma warning(pop)

#include <memory>

namespace Budgety
{
	class Entity;
	
	class Component
	{
	public:
		virtual void Init() {} //override when you need to access the Entity (or Scene, if Entity is already in a Scene!!)
		virtual void Update() = 0;
		virtual void Render() const = 0;
		
		Component() = default;
		virtual ~Component() = default;
		
		Component(const Component& other) = delete;
		Component(Component&& other) = delete;
		Component& operator=(const Component& other) = delete;
		Component& operator=(Component&& other) = delete;
		
		inline void SetEntity(Entity* pEntity) { m_pEntity = pEntity; }

		inline void SetIsActive(bool isActive) { m_IsActive = isActive; }
		inline bool GetIsActive() const { return m_IsActive; }

		inline Entity* GetEntity() const { return m_pEntity; }

	private:
		Entity* m_pEntity = nullptr;

		bool m_IsActive = true;
	};
}