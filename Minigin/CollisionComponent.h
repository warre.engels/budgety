#pragma once
#include "Component.h"
#include "Utils.h"
#include <unordered_set>

namespace Budgety
{
	class Texture2DComponent;
	class Entity;
	
	enum class CollisionGroup
	{
		Player,
		Ground,
		Enemy,
		Bubble
	};

	//side where this was touched
	enum class Side
	{	//don't change values
		Left = 0,
		Right = 1,
		Top = 2,
		Bottom = 3
	};
	
	class CollisionComponent : public Component, public std::enable_shared_from_this<CollisionComponent>
	{
	public:
		explicit CollisionComponent(CollisionGroup group, const std::shared_ptr<Texture2DComponent>& pTextureComp);
		explicit CollisionComponent(CollisionGroup group, const glm::vec2& dimensions, const glm::vec2& pivot = { 0.5f, 0.5f });
		explicit CollisionComponent(CollisionGroup group, int width, int height, const glm::vec2& pivot = { 0.5f, 0.5f });
		virtual ~CollisionComponent() = default;
		
		void Init() override;
		void Update() override;
		void Render() const override;

		inline CollisionGroup GetCollisionGroup() const { return m_Group; }
		inline const SDL_Rect& GetRect() const { return m_Rect; }

		bool IsColliding(const Entity* pEntity) const;
		
		virtual void OnCollisionEnter(Entity* pOtherEntity, Budgety::Side enterSide);
		virtual void OnCollisionExit(Entity* pOtherEntity);

	protected:
		inline const glm::vec2& GetRelativePos() const { return m_RelativePosition; }
		
		std::unordered_set<const Entity*> m_pEnteredEntities;
		
	private:
		CollisionGroup m_Group;
		glm::vec2 m_RelativePosition;
		SDL_Rect m_Rect;
	};
}