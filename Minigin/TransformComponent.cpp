#include "MiniginPCH.h"
#include "TransformComponent.h"

Budgety::TransformComponent::TransformComponent(float x, float y, float z)
	: TransformComponent(glm::vec3{ x, y, z })
{}

Budgety::TransformComponent::TransformComponent(const glm::vec2& pos, float z)
	: TransformComponent(glm::vec3{ pos.x, pos.y, z })
{}

Budgety::TransformComponent::TransformComponent(glm::vec3 pos)
	: Component()
	, m_Position(std::move(pos))
{
}

void Budgety::TransformComponent::Update()
{
	
}

void Budgety::TransformComponent::Render() const
{
	
}