#pragma once
#include "Singleton.h"
#include "Command.h"
#include <XInput.h>
#include <SDL.h>

namespace Budgety
{	
	enum class Button
	{
		DPadUp = XINPUT_GAMEPAD_DPAD_UP,
		DPadDown = XINPUT_GAMEPAD_DPAD_DOWN,
		DPadLeft = XINPUT_GAMEPAD_DPAD_LEFT,
		DPadRight = XINPUT_GAMEPAD_DPAD_RIGHT,
		Start = XINPUT_GAMEPAD_START,
		Back = XINPUT_GAMEPAD_BACK,
		LeftThumbStick = XINPUT_GAMEPAD_LEFT_THUMB,
		RightThumbStick = XINPUT_GAMEPAD_RIGHT_THUMB,
		LeftShoulder = XINPUT_GAMEPAD_LEFT_SHOULDER,
		RightShoulder = XINPUT_GAMEPAD_RIGHT_SHOULDER,
		ButtonA = XINPUT_GAMEPAD_A,
		ButtonB = XINPUT_GAMEPAD_B,
		ButtonX = XINPUT_GAMEPAD_X,
		ButtonY = XINPUT_GAMEPAD_Y
	};

	class InputManager final : public Singleton<InputManager>
	{
	public:
		enum class TriggerType
		{
			Press,
			Release,
			Hold
		};
		
	private:
		const struct KeyTrigger
		{
			const int key;
			const TriggerType type;
			const std::shared_ptr<Command> pCommand;
			Entity* pEntity;

			KeyTrigger(const int sdlScanCode, const TriggerType triggerType, std::shared_ptr<Command> pComm, Entity* pEnt)
				: key{ sdlScanCode }
				, type{ triggerType }
				, pCommand{ std::move(pComm) }
				, pEntity{ pEnt }
			{}
		};
		
		const struct ButtonTrigger
		{
			const unsigned int contIdx;
			const Button button;
			const TriggerType type;
			const std::shared_ptr<Command> pCommand;
			Entity* pEntity;

			ButtonTrigger(unsigned int controllerIdx, const Button triggerButton, const TriggerType triggerType, std::shared_ptr<Command> pComm, Entity* pEnt)
				: contIdx{ controllerIdx }
				, button{ triggerButton }
				, type{ triggerType }
				, pCommand{ std::move(pComm) }
				, pEntity{ pEnt }
			{
				if (controllerIdx >= XUSER_MAX_COUNT)
					throw std::exception(("XINPUT doesn't support more then 4 players, playerIdx '" + std::to_string(controllerIdx) + "' was attempted to be assigned").c_str());
			}
		};
		
		struct KeyboardState
		{
			int keyboardStateSize;
			Uint8* prev;
			const Uint8* curr;

			KeyboardState() : curr{ SDL_GetKeyboardState(&keyboardStateSize) }, prev{ nullptr }
			{
				prev = new Uint8[keyboardStateSize];
			}
			~KeyboardState()
			{
				delete[] prev;
			}
		};
		struct ControllerState
		{
			XINPUT_STATE prev, curr;

			ControllerState(XINPUT_STATE current) : prev{}, curr { current } {}
			ControllerState() : prev{}, curr{} { ZeroMemory(&curr, sizeof(XINPUT_STATE)); }
		};

	public:
		bool ProcessInput();

		//sdlScanCode: key (example: SDL_SCANCODE_A)
		void AddCommand(int sdlScanCode, TriggerType type, std::shared_ptr<Command> pCommand, Entity* pEntity);
		
		inline void AddCommand(Button button, TriggerType type, std::shared_ptr<Command> pCommand, Entity* pEntity)
		{ AddCommand(0, button, type, std::move(pCommand), std::move(pEntity)); }
		void AddCommand(unsigned int controllerIdx, Button button, TriggerType type, std::shared_ptr<Command> pCommand, Entity* pEntity);

	private:
		friend class Singleton<InputManager>;
		InputManager();
		~InputManager();

		typedef bool (InputManager::*KeyTriggeredFunc)(const std::shared_ptr<KeyTrigger>&) const;
		typedef bool (InputManager::*ButtonTriggeredFunc)(const std::shared_ptr<ButtonTrigger>&) const;

		bool IsPressed(const std::shared_ptr<KeyTrigger>& pKeyTrigger) const;
		bool IsReleased(const std::shared_ptr<KeyTrigger>& pKeyTrigger) const;
		bool IsHeld(const std::shared_ptr<KeyTrigger>& pKeyTrigger) const;

		bool IsPressed(const std::shared_ptr<ButtonTrigger>& pButtonTrigger) const;
		bool IsReleased(const std::shared_ptr<ButtonTrigger>& pButtonTrigger) const;
		bool IsHeld(const std::shared_ptr<ButtonTrigger>& pButtonTrigger) const;
		
		std::vector<ControllerState> m_ControllerStates;
		KeyboardState m_KeyboardState;

		std::vector<std::shared_ptr<KeyTrigger>> m_pKeyTriggers;
		std::vector<std::shared_ptr<ButtonTrigger>> m_pButtonTriggers;
	};
}