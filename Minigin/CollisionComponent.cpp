#include "MiniginPCH.h"
#include "CollisionComponent.h"
#include "Components.h"
#include "Entity.h"
#include "Texture2D.h"
#include "Scene.h"
#include "Renderer.h"

Budgety::CollisionComponent::CollisionComponent(CollisionGroup group, const std::shared_ptr<Texture2DComponent>& pTextureComp)
	: CollisionComponent(group, pTextureComp->GetRect().w, pTextureComp->GetRect().h, pTextureComp->GetPivot())
{}

Budgety::CollisionComponent::CollisionComponent(CollisionGroup group, const glm::vec2& dimensions, const glm::vec2& pivot)
	: CollisionComponent(group, int(dimensions.x), int(dimensions.y), pivot)
{}

Budgety::CollisionComponent::CollisionComponent(CollisionGroup group, int width, int height, const glm::vec2& pivot)
	: Component()
	, m_Group{ group }
	, m_RelativePosition{ -width * pivot.x, -height * pivot.y }
	, m_Rect{}
{
	m_Rect.w = width;
	m_Rect.h = height;
}

void Budgety::CollisionComponent::Init()
{
	auto pScene = GetEntity()->GetScene();
	if (pScene == nullptr)
		throw std::exception("CollisionComponent::Init(): Entity must be added to Scene before adding CollisionComponent");

	pScene->AddCollisionComponent(shared_from_this());
}

void Budgety::CollisionComponent::Update()
{
	auto pos = m_RelativePosition + GetEntity()->GetComponent<TransformComponent>()->Get2DPosition();
	m_Rect.x = int(pos.x);
	m_Rect.y = int(pos.y);
}

void Budgety::CollisionComponent::Render() const
{
#if _DEBUG
	if(true)
	{
		SDL_SetRenderDrawColor(Renderer::GetInstance().GetSDLRenderer(), 255, 0, 255, 255);
		SDL_RenderDrawRect(Renderer::GetInstance().GetSDLRenderer(), &m_Rect);
	}
#endif
}

bool Budgety::CollisionComponent::IsColliding(const Budgety::Entity* pOtherEntity) const
{
	auto it = std::find_if(m_pEnteredEntities.cbegin(), m_pEnteredEntities.cend(),
		[pOtherEntity](const Entity* pEntity)
		{
			return pEntity == pOtherEntity;
		});

	return it != m_pEnteredEntities.end();
}

void Budgety::CollisionComponent::OnCollisionEnter(Budgety::Entity* pOtherEntity, Budgety::Side enterSide)
{
	UNREFERENCED_PARAMETER(enterSide);
	m_pEnteredEntities.insert(pOtherEntity);

	//std::cout << "Entered collision\n";
}

void Budgety::CollisionComponent::OnCollisionExit(Budgety::Entity* pOtherEntity)
{
	for (auto it = m_pEnteredEntities.begin(); it != m_pEnteredEntities.end();)
	{
		if (*it == nullptr || *it == pOtherEntity)
		{
			it = m_pEnteredEntities.erase(it);
		}
		else
		{
			++it;
		}
	}

	//std::cout << "Exited collision\n";
}