#include "MiniginPCH.h"
#include "Texture2D.h"
#include <SDL.h>

Budgety::Texture2D::Texture2D(SDL_Texture* pTexture)
{
	m_pTexture = pTexture;

	SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_Width, &m_Height);
}

Budgety::Texture2D::~Texture2D()
{
	SDL_DestroyTexture(m_pTexture);
}

SDL_Texture* Budgety::Texture2D::GetSDLTexture() const
{
	return m_pTexture;
}

glm::vec2 Budgety::Texture2D::GetDimensions() const
{
	return glm::vec2(m_Width, m_Height);
}