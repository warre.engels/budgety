#include "MiniginPCH.h"
#include "Minigin.h"
#include <chrono>
#include <thread>
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include <SDL.h>
#include "Scene.h"
#include "Entity.h"
//Components
#include "TransformComponent.h"
#include "Texture2DComponent.h"
#include "TextComponent.h"
#include "FPSComponent.h"

using namespace std::chrono;

void Budgety::Minigin::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	m_pWindow = SDL_CreateWindow(
		"BubbleBobble - Engels Warre",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		WINDOW_WIDTH,
		WINDOW_HEIGHT,
		SDL_WINDOW_OPENGL
	);
	if (m_pWindow == nullptr)
	{
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	Renderer::GetInstance().Init(m_pWindow);
}

void Budgety::Minigin::Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(m_pWindow);
	m_pWindow = nullptr;
	SDL_Quit();
}

void Budgety::Minigin::Run(void(*LoadGame)())
{
	Initialize();

	// tell the resource manager where it can find the game data
	ResourceManager::GetInstance().Init("../Data/");
	
	LoadGame();

	{
		auto& time = Time::GetInstance();
		auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();
		auto& input = InputManager::GetInstance();

#ifdef FRAMERATECAP
		time.SetElapsed(secPerFrame);
		float storedTime = 0.f;
#endif
		bool doContinue = true;
		auto lastTime = time.GetCurrent();

		while (doContinue)
		{
			const auto currentTime = time.GetCurrent();
			float deltaTime = duration<float, std::micro>(currentTime - lastTime).count() / std::micro::den;
			lastTime = currentTime;
#ifdef FRAMERATECAP
			storedTime += deltaTime;
			while (storedTime >= secPerFrame)
			{
#else
				time.SetElapsed(deltaTime);
#endif
				doContinue = input.ProcessInput();
				sceneManager.Update();
#ifdef FRAMERATECAP
				storedTime -= secPerFrame;
			}
#endif
			renderer.Render();
		}
	}

	Cleanup();
}