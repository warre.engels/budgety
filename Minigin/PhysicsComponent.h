#pragma once
#include "Component.h"

namespace Budgety
{
	class PhysicsComponent final : public Component
	{
	public:
		explicit PhysicsComponent(float mass, float terminalVelocity = 500.f, float g = 9.81f, float airResistance = 150.f);
		void Update() override;
		void Render() const override;

		inline const glm::vec3& GetVelocity() const { return m_Velocity; }
		
		inline void SetVelocity(float x, float y, float z) { SetVelocity(glm::vec3{ x, y, z }); }
		inline void SetVelocity(const glm::vec3& velocity) { m_Velocity = velocity; }

		inline void EnableGravity() { m_IsGravityEnabled = true; }
		inline void DisableGravity() { m_IsGravityEnabled = false; }
		
		inline void AddForce(float x, float y, float z) { AddForce(glm::vec3{ x, y, z }); }
		void AddForce(const glm::vec3& force) { m_Velocity += force; }

	private:
		float m_Mass;
		float m_TerminalVel;
		float m_G;
		float m_AirResistance;

		glm::vec3 m_Velocity;
		bool m_IsGravityEnabled;
	};
}