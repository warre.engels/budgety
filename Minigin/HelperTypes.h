#pragma once
#pragma warning(push)
//nonstandard extension used : nameless struct/union
#pragma warning (disable : 4201)
#pragma warning (disable : 26812)
#pragma warning (disable : 26495)
#include <glm/vec2.hpp>
#pragma warning(pop)

namespace Budgety
{
	class Entity;
}

#define FRAMERATECAP 60

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

#define AMOUNT_OF_LEVELS 100
#define SCALE 2.f
#define BLOCKSIZE 8
#define SPRITESIZE 16
//must be dividable by 8
#define LEVELWIDTH 32
#define LEVELHEIGHT 25

constexpr float levelDisplacementX = (WINDOW_WIDTH - (LEVELWIDTH * BLOCKSIZE * SCALE)) / 2;
constexpr float levelDisplacementY = (WINDOW_HEIGHT - (LEVELHEIGHT * BLOCKSIZE * SCALE)) / 2;

constexpr float player1SpawnPosX = levelDisplacementX + 4 * BLOCKSIZE * SCALE;
constexpr float player2SpawnPosX = levelDisplacementX + (LEVELWIDTH - 4) * BLOCKSIZE * SCALE;
constexpr float playerSpawnPosY = levelDisplacementY + (LEVELHEIGHT - 3) * BLOCKSIZE * SCALE;

//sprites.png animation indices
//call AddAnimation() in the same order
enum class PlayerAnimation
{
	Movingright = 0,
	Movingleft = 1,
	Shootright,
	Shootleft,
	Death
};
//sprites.png animation indices
//call AddAnimation() in the same order
enum class EnemyAnimation
{
	Movingright = 0,
	Movingleft = 1,
	Death,
	InBubble
};

enum class EnemyType
{
	ZenChan,
	Maita
};

struct Bubble
{
	Bubble(Budgety::Entity* pBubble) : pBubble{ pBubble }, accSec{ 0.f } {}
	
	Budgety::Entity* pBubble = nullptr;
	float accSec = 0.f;
	const static float maxSec;
};