#include "MiniginPCH.h"
#include "Time.h"

using namespace std::chrono;

Budgety::Time::Time()
	: m_ElapsedSec{}
{
	
}

steady_clock::time_point Budgety::Time::GetCurrent() const
{
	return high_resolution_clock::now();
}

void Budgety::Time::PrintDuration(const steady_clock::time_point& start, const steady_clock::time_point& end, const std::string& text) const
{
	const float ms = duration_cast<microseconds>(end - start).count() / 1000.f;
	std::cout << "'" << text << "': " << ms << " ms\n";
}

float Budgety::Time::GetElapsed() const
{
	return m_ElapsedSec;
}

void Budgety::Time::SetElapsed(float elapsedSec)
{
	m_ElapsedSec = elapsedSec;
}
