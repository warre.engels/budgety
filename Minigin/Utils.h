#pragma once
#include "HelperTypes.h"
#include <sstream>
#include <SDL.h>

namespace Budgety
{
	enum class Side;
}

namespace utils
{
	std::string to_string_with_precision(const float value, const int precision = 1);

	bool MouseInRect(const glm::vec2& mousePos, const SDL_Rect& rect);
	bool IsOverlapping(const int expandAmount, const SDL_Rect& rect1, const SDL_Rect& rect2);
	bool IsOverlapping(const SDL_Rect& rect1, const SDL_Rect& rect2);
	Budgety::Side GetOppositeSide(const Budgety::Side side);
}