#include "MiniginPCH.h"
#include "SceneManager.h"
#include "Scene.h"

void Budgety::SceneManager::Update()
{
	if(m_pActiveScene == nullptr)
		throw std::exception("SceneManager: no active scene");
	
	m_pActiveScene->Update();
}

void Budgety::SceneManager::Render()
{
	if (m_pActiveScene == nullptr)
		throw std::exception("SceneManager: no active scene");
	
	m_pActiveScene->Render();
}

void Budgety::SceneManager::SetActiveScene(const std::string& name)
{
	for (const std::shared_ptr<Scene>& pScene : m_pScenes)
	{
		if (pScene->GetName() == name)
		{
			m_pActiveScene = pScene.get();
			m_pActiveScene->Resume();
			return;
		}
	}

	throw std::exception(("SceneManager::SetActiveScene(): Scene with name '" + name + "' not found").c_str());
}

Budgety::Scene& Budgety::SceneManager::CreateScene(std::string name)
{
	//new used because Constructor is private
	const auto pScene = std::shared_ptr<Scene>(new Scene(std::move(name)));
	m_pScenes.push_back(pScene);
	return *pScene;
}