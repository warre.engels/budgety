#include "MiniginPCH.h"
#include "InputManager.h"
#include "SceneManager.h"
#include "Entity.h"

Budgety::InputManager::InputManager()
	: m_ControllerStates{}
	, m_KeyboardState{}
	, m_pKeyTriggers{}
	, m_pButtonTriggers{}
{
	DWORD dwResult;
	for (DWORD i{ 0 }; i < XUSER_MAX_COUNT; i++)
	{
		XINPUT_STATE state;
		ZeroMemory(&state, sizeof(XINPUT_STATE));
		dwResult = XInputGetState(i, &state);

		if (dwResult == ERROR_SUCCESS)
		{
			//add controllerState and put curr on 'state'
			m_ControllerStates.push_back(state);
		}
	}
	std::cout << m_ControllerStates.size() << " controllers connected\n";
}

Budgety::InputManager::~InputManager()
{

}

bool Budgety::InputManager::ProcessInput()
{
	//Controller
	for (size_t i{ 0 }; i < m_ControllerStates.size(); i++)
	{
		m_ControllerStates[i].prev = std::move(m_ControllerStates[i].curr);
		ZeroMemory(&m_ControllerStates[i].curr, sizeof(XINPUT_STATE));
		XInputGetState(0, &m_ControllerStates[i].curr);
	}

	//Keyboard
	memcpy(m_KeyboardState.prev, m_KeyboardState.curr, m_KeyboardState.keyboardStateSize);
	SDL_PumpEvents();
	m_KeyboardState.curr = SDL_GetKeyboardState(NULL);

	//Trigger commands (if they are in the active scene)
	KeyTriggeredFunc IsKeyTriggeredFunc{ nullptr };
	ButtonTriggeredFunc IsButtonTriggeredFunc{ nullptr };
	Scene* pActiveScene = SceneManager::GetInstance().GetActiveScene();
	Entity* pEntity{ nullptr };
	
	for(const std::shared_ptr<KeyTrigger>& pKeyTrigger : m_pKeyTriggers)
	{
		pEntity = pKeyTrigger->pEntity;
		if (pEntity->GetScene() != pActiveScene)
			continue; //not in the active Scene
		
		switch (pKeyTrigger->type)
		{
		case TriggerType::Press:
			IsKeyTriggeredFunc = &InputManager::IsPressed;
			break;
		case TriggerType::Release:
			IsKeyTriggeredFunc = &InputManager::IsReleased;
			break;
		case TriggerType::Hold:
			IsKeyTriggeredFunc = &InputManager::IsHeld;
			break;
		}
		
		if ((this->*IsKeyTriggeredFunc)(pKeyTrigger))
			pKeyTrigger->pCommand->Execute(pEntity);
	}
	for(const std::shared_ptr<ButtonTrigger>& pButtonTrigger : m_pButtonTriggers)
	{
		pEntity = pButtonTrigger->pEntity;
		if (pEntity->GetScene() != pActiveScene)
			continue; //not in the active Scene
		
		switch (pButtonTrigger->type)
		{
		case TriggerType::Press:
			IsButtonTriggeredFunc = &InputManager::IsPressed;
			break;
		case TriggerType::Release:
			IsButtonTriggeredFunc = &InputManager::IsReleased;
			break;
		case TriggerType::Hold:
			IsButtonTriggeredFunc = &InputManager::IsHeld;
			break;
		}
		
		if((this->*IsButtonTriggeredFunc)(pButtonTrigger))
			pButtonTrigger->pCommand->Execute(pEntity);
	}
	
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT)
		{
			return false;
		}
	}

	return true;
}

void Budgety::InputManager::AddCommand(int sdlScanCode, TriggerType type, std::shared_ptr<Command> pCommand, Entity* pEntity)
{
	m_pKeyTriggers.push_back(std::make_unique<KeyTrigger>(sdlScanCode, type, std::move(pCommand), std::move(pEntity)));
}

void Budgety::InputManager::AddCommand(unsigned int controllerIdx, Button button, TriggerType type, std::shared_ptr<Command> pCommand, Entity* pEntity)
{
	//first time you try to add a command for the controller
	if(m_pButtonTriggers.size() == 0)
		if (controllerIdx >= m_ControllerStates.size())
			std::cout << "not enough controllers to experience all features of this game without a keyboard\n";
	
	m_pButtonTriggers.push_back(std::make_unique<ButtonTrigger>(controllerIdx, button, type, std::move(pCommand), std::move(pEntity)));
}

///////////////////////////////////////////////////////
// functions to check if you have to trigger command //
///////////////////////////////////////////////////////

bool Budgety::InputManager::IsPressed(const std::shared_ptr<KeyTrigger>& pKeyTrigger) const
{
	bool currDown = m_KeyboardState.curr[pKeyTrigger->key];
	bool prevDown = m_KeyboardState.prev[pKeyTrigger->key];

	return !prevDown && currDown;
}

bool Budgety::InputManager::IsReleased(const std::shared_ptr<KeyTrigger>& pKeyTrigger) const
{
	bool currDown = m_KeyboardState.curr[pKeyTrigger->key];
	bool prevDown = m_KeyboardState.prev[pKeyTrigger->key];

	return prevDown && !currDown;
}

bool Budgety::InputManager::IsHeld(const std::shared_ptr<KeyTrigger>& pKeyTrigger) const
{
	bool currDown = m_KeyboardState.curr[pKeyTrigger->key];

	return currDown;
}

bool Budgety::InputManager::IsPressed(const std::shared_ptr<ButtonTrigger>& pButtonTrigger) const
{
	size_t controllerIdx = size_t(pButtonTrigger->contIdx);

	if (controllerIdx >= m_ControllerStates.size())
		return false;
	
	bool prevDown = m_ControllerStates[controllerIdx].prev.Gamepad.wButtons & int(pButtonTrigger->button);
	bool currDown = m_ControllerStates[controllerIdx].curr.Gamepad.wButtons & int(pButtonTrigger->button);
	
	return !prevDown && currDown;
}

bool Budgety::InputManager::IsReleased(const std::shared_ptr<ButtonTrigger>& pButtonTrigger) const
{
	size_t controllerIdx = size_t(pButtonTrigger->contIdx);

	if (controllerIdx >= m_ControllerStates.size())
		return false;

	bool prevDown = m_ControllerStates[controllerIdx].prev.Gamepad.wButtons & int(pButtonTrigger->button);
	bool currDown = m_ControllerStates[controllerIdx].curr.Gamepad.wButtons & int(pButtonTrigger->button);

	return prevDown && !currDown;
}

bool Budgety::InputManager::IsHeld(const std::shared_ptr<ButtonTrigger>& pButtonTrigger) const
{
	size_t controllerIdx = size_t(pButtonTrigger->contIdx);

	if (controllerIdx >= m_ControllerStates.size())
		return false;

	bool currDown = m_ControllerStates[controllerIdx].curr.Gamepad.wButtons & int(pButtonTrigger->button);

	return currDown;
}