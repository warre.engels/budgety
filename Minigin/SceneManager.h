#pragma once
#include "Singleton.h"

namespace Budgety
{
	class Scene;
	
	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		Scene& CreateScene(std::string name);

		void Update();
		void Render();

		inline Scene* GetActiveScene() const { return m_pActiveScene; }
		void SetActiveScene(const std::string& name);
		
	private:
		friend class Singleton<SceneManager>;
		SceneManager() = default;

		std::vector<std::shared_ptr<Scene>> m_pScenes{};
		Scene* m_pActiveScene = nullptr;
	};
}
