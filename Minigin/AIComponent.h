#pragma once
#include "Component.h"

namespace Budgety
{
	class AIComponent final : public Component
	{
	public:
		AIComponent();
		explicit AIComponent(std::vector<const Entity*> pTargets);
		void Update() override;
		void Render() const override;

	private:
		std::vector<const Entity*> m_pTargets;
	};
}