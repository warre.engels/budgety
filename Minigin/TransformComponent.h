#pragma once
#pragma warning(push)
//nonstandard extension used : nameless struct/union
#pragma warning (disable : 4201)
#pragma warning (disable : 26812)
#pragma warning (disable : 26495)
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#pragma warning(pop)

#include "Component.h"

namespace Budgety
{
	class TransformComponent final : public Component
	{
	public:
		explicit TransformComponent(float x = 0.f, float y = 0.f, float z = 0.f);
		explicit TransformComponent(const glm::vec2& pos, float z = 0.f);
		explicit TransformComponent(glm::vec3 pos);
		
		void Update() override;
		void Render() const override;
		
		inline const glm::vec3& GetPosition() const { return m_Position; }
		inline glm::vec2 Get2DPosition() const { return { m_Position.x, m_Position.y }; }
		
		inline void SetPosition(float x, float y, float z = 0.f) { SetPosition({ x, y, z }); }
		inline void SetPosition(const glm::vec2& pos, float z = 0.f) { SetPosition({ pos.x, pos.y, z }); }
		inline void SetPosition(glm::vec3 pos) { m_Position = std::move(pos); }
		inline void Set2DPosition(const glm::vec2& pos) { m_Position.x = pos.x; m_Position.y = pos.y; }

	private:
		glm::vec3 m_Position;
	};
}