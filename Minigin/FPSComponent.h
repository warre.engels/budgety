#pragma once
#include "Component.h"
#include "TextComponent.h"


namespace Budgety
{
	class TextComponent;

	class FPSComponent final : public Component
	{
	public:
		FPSComponent();
		void Update() override;
		void Render() const override;

		inline int GetFPS() const { return m_FPS; }
		std::shared_ptr<TextComponent> AddTextComponent(std::shared_ptr<TextComponent> pText);

	private:
		int m_FPS;
		float m_LastRefeshTime;
	};
}