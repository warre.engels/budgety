#include "MiniginPCH.h"
#include "Texture2DComponent.h"

#include "Texture2D.h"
#include "ResourceManager.h"
#include "Renderer.h"

#include "Entity.h"
#include "TransformComponent.h"

Budgety::Texture2DComponent::Texture2DComponent(const std::string& path, glm::vec2 pivot, float scale)
	: Component()
	, m_Pivot{ std::move(pivot) }
	, m_Rect{}
	, m_Scale{ scale }
	, m_Width{}
	, m_Height{}
	, m_SrcRect{}
	, m_Type{ Type::all }
	, m_pAnimationDatas{}
	, m_CurrAnimIdx{ -1 }
{	
	m_pTexture = ResourceManager::GetInstance().LoadTexture(path);
}

Budgety::Texture2DComponent::Texture2DComponent(const std::string& path, int width, int height, glm::vec2 pivot, float scale)
	: Texture2DComponent(path, pivot, scale)
{
	m_Type = Type::part;
	m_Width = width;
	m_Height = height;
}

void Budgety::Texture2DComponent::Init()
{

	switch (m_Type)
	{
	case Type::all:
		CalculateRect(true, true);
		break;
		
	case Type::part:
		CalculateRect(false, true);
		break;
		
	case Type::animated:
		CalculateRect(false, true);
		break;
	}
}

void Budgety::Texture2DComponent::Update()
{
	switch (m_Type)
	{
	case Type::all:
		CalculateRect(true);
		break;
		
	case Type::part:
		CalculateRect(false);
		break;
		
	case Type::animated:
		CalculateRect(false);
		
		m_pAnimationDatas[m_CurrAnimIdx]->accSec += Time::GetInstance().GetElapsed();
		if (m_pAnimationDatas[m_CurrAnimIdx]->accSec > m_pAnimationDatas[m_CurrAnimIdx]->frameTime)
		{
			++m_pAnimationDatas[m_CurrAnimIdx]->currFrame %= m_pAnimationDatas[m_CurrAnimIdx]->nrFrames;
			m_pAnimationDatas[m_CurrAnimIdx]->accSec -= m_pAnimationDatas[m_CurrAnimIdx]->frameTime;

			//update srcRect
			m_SrcRect.x = (m_pAnimationDatas[m_CurrAnimIdx]->currFrame % m_pAnimationDatas[m_CurrAnimIdx]->nrFrames) * (m_SrcRect.w + m_pAnimationDatas[m_CurrAnimIdx]->frameOffset);
		}
		break;
	}
}

void Budgety::Texture2DComponent::Render() const
{
	if (m_pTexture == nullptr)
		return;
	
	if (!GetEntity()->HasComponent<TransformComponent>())
		throw std::exception("Texture2DComponent: TransformComponent not found");

	if (m_Type == Type::all)
		Renderer::GetInstance().RenderTexture(*m_pTexture, m_Rect);
	else
		Renderer::GetInstance().RenderTexture(*m_pTexture, m_Rect, &m_SrcRect);
}

void Budgety::Texture2DComponent::AddAnimation(const int nrFrames, const int srcRectWidth, const int srcRectHeight, const float frameTime, const int topMargin, const int frameOffset)
{
	m_Type = Type::animated;
	m_pAnimationDatas.push_back(std::make_unique<AnimationData>(frameTime, nrFrames, srcRectWidth, srcRectHeight, topMargin, frameOffset));

	if (m_pAnimationDatas.size() == 1)
		SetAnimation(0);
}

void Budgety::Texture2DComponent::SetAnimation(int idx)
{
	if (m_CurrAnimIdx == idx)
		return;
	
	m_CurrAnimIdx = idx;

	m_pAnimationDatas[m_CurrAnimIdx]->accSec = 0.f;
	m_pAnimationDatas[m_CurrAnimIdx]->currFrame = 0;

	m_SrcRect.y = m_pAnimationDatas[m_CurrAnimIdx]->topMargin;
	m_SrcRect.w = m_pAnimationDatas[m_CurrAnimIdx]->srcRectWidth;
	m_SrcRect.h = m_pAnimationDatas[m_CurrAnimIdx]->srcRectHeight;
}

void Budgety::Texture2DComponent::CalculateRect(bool useTexDim, bool calcSrcRect)
{
	auto pos = GetEntity()->GetComponent<TransformComponent>()->Get2DPosition();
	auto dimensions = useTexDim ? m_pTexture->GetDimensions() : glm::vec2{ m_Width, m_Height };
	if(calcSrcRect)
	{
		m_SrcRect.x = 0;
		m_SrcRect.y = 0;
		m_SrcRect.w = static_cast<int>(dimensions.x);
		m_SrcRect.h = static_cast<int>(dimensions.y);
	}
	dimensions *= m_Scale;
	pos -= (m_Pivot * dimensions);

	m_Rect.x = static_cast<int>(pos.x);
	m_Rect.y = static_cast<int>(pos.y);
	m_Rect.w = static_cast<int>(dimensions.x);
	m_Rect.h = static_cast<int>(dimensions.y);
}