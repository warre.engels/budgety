#pragma once
#include "Component.h"
#include <string>
#include "Utils.h"

namespace Budgety
{
	class Texture2D;
	
	class Texture2DComponent final : public Component
	{
	public:
		explicit Texture2DComponent(const std::string& path, glm::vec2 pivot = { 0.f, 0.f }, float scale = 1.f);
		explicit Texture2DComponent(const std::string& path, int srcRectWidth, int srcRectHeight, glm::vec2 pivot = { 0.f, 0.f }, float destRectScale = 1.f);
		~Texture2DComponent() = default;

		Texture2DComponent(const Texture2DComponent&) = delete;
		Texture2DComponent(Texture2DComponent&&) = delete;
		Texture2DComponent& operator= (const Texture2DComponent&) = delete;
		Texture2DComponent& operator= (const Texture2DComponent&&) = delete;

		void Init() override;
		void Update() override;
		void Render() const override;

		inline std::shared_ptr<Texture2D> GetTexture() const { return m_pTexture; }
		inline const glm::vec2& GetPivot() const { return m_Pivot; }
		inline const SDL_Rect& GetRect() const { return m_Rect; }
		inline float GetScale() const { return m_Scale; }
		inline int GetAnimation() const { return m_CurrAnimIdx; }

		inline void SetPivot(float x, float y) { SetPivot(glm::vec2(x, y)); }
		inline void SetPivot(glm::vec2 pivot) { m_Pivot = std::move(pivot); }
		inline void SetSrcRectPos(glm::vec2 pos) { SetSrcRectPos(int(pos.x), int(pos.y)); }
		inline void SetSrcRectPos(int x, int y) { m_SrcRect.x = x; m_SrcRect.y = y; }

		void AddAnimation(const int nrFrames, const int srcRectWidth, const int srcRectHeight, const float frameTime = 0.2f, const int topMargin = 0, const int frameOffset = 0);
		void SetAnimation(int idx);

	private:
		//what part of the texture you want to capture
		enum class Type
		{
			all,
			part,
			animated
		};

		struct AnimationData
		{
			float accSec;
			const float frameTime;
			int currFrame;
			const int nrFrames;
			const int srcRectWidth;
			const int srcRectHeight;
			const int topMargin;
			const int frameOffset;

			AnimationData(const float frameTime, const int nrFrames, const int srcRectWidth, const int srcRectHeight, const int topMargin, const int frameOffset)
				: accSec{ 0.f }
				, frameTime{ frameTime }
				, currFrame{ 0 }
				, nrFrames{ nrFrames }
				, srcRectWidth{ srcRectWidth }
				, srcRectHeight{ srcRectHeight }
				, topMargin{ topMargin }
				, frameOffset{ frameOffset }
			{}
		};
		
		void CalculateRect(bool useTexDim, bool calcSrcRect = false);
		
		std::shared_ptr<Texture2D> m_pTexture;
		glm::vec2 m_Pivot;
		SDL_Rect m_Rect;
		float m_Scale;
		int m_Width, m_Height;

		SDL_Rect m_SrcRect;
		Type m_Type;
		std::vector<std::unique_ptr<AnimationData>> m_pAnimationDatas;
		int m_CurrAnimIdx;
	};
}